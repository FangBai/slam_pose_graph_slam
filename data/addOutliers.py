#!/usr/bin/python

# This script is used to add outliers to a dataset
# use the script by the syntax:
# ------------------------------------------- #
# python <addOutliersIncremental.py> <input_data_file> <input_solution_file> <output_data_file> <outlier_num>
# ------------------------------------------- #
# Fang Bai @ Jan-31-2018
# University of Technology Sydney
# Fang.Bai@student.uts.edu.au


import sys, os, random
from math import sin, cos, atan2
from shutil import copyfile


def main():

    if len(sys.argv) < 5:
        print ("Please use the script in the format: \n")
        print ("python <script_name> <input_data_filename> <input_solution_filename> <output_data_filename> <outlier_num>\n")
        sys.exit()

    input_data_file = sys.argv[1]
    input_solution_file = sys.argv[2]
    output_data_file = sys.argv[3]
    outlier_num = int(sys.argv[4])

    if not os.path.isfile(input_data_file):
        print ("Input data file {} does not exist.".format(input_data_file))
        sys.exit()

    if not os.path.isfile(input_solution_file):
        print ("Input solution file {} does not exist.".format(input_solution_file))
        sys.exit()

    # get correct vertices
    vertices_stack = list()
    with open(input_solution_file) as fsinp:
        for line in fsinp:
            chline = line.split(' ')
            vertices_stack.append( ( float(chline[2]), float(chline[3]), float(chline[4]) ) )

    # get correct loop-closure edges into inlier_loop_closure_stack
    inlier_loop_closure_stack = list()
    with open(input_data_file) as fdinp:
        for line in fdinp:
            chline = line.split(' ')
            prefix = chline[0]
            if(prefix[0:4]=='EDGE'):
                id1 = int(chline[1])
                id2 = int(chline[2])
                if (not (abs(id1-id2)==1)):
                    inlier_loop_closure_stack.append( (id1, id2) )

    # lower bound and upper bound for vertex index
    lb = 0
    ub = len(vertices_stack) - 1

    # stack to preserve generated outliers
    outlier_loop_closure_stack = list()
    n_outlier = 0
    while(n_outlier < outlier_num):
        # generate an edge id, and ensure it does not exist yet
        outlier_edge = RandomPair(lb, ub)
        while( ExistLoopClosure(outlier_edge[0], outlier_edge[1], inlier_loop_closure_stack, outlier_loop_closure_stack) ):
            outlier_edge = RandomPair(lb, ub)
        # compute correct measurement
        measurement_correct = MimickMeasurement2D(vertices_stack[outlier_edge[0]], vertices_stack[outlier_edge[1]])
        # randomly select two poses and compute the fake measurement
        id_pair = RandomPair(lb, ub)
        measurement_fake = MimickMeasurement2D(vertices_stack[id_pair[0]], vertices_stack[id_pair[1]])
        # ensure that the correct measurement and fake measurement are not too close
        while ( IsMeasurementClose2D(measurement_correct, measurement_fake) ):
            id_pair = RandomPair(lb, ub)
            measurement_fake = MimickMeasurement2D(vertices_stack[id_pair[0]], vertices_stack[id_pair[1]])
        # push the outlier edge into the stack
        outlier_loop_closure_stack.append( [ outlier_edge, measurement_fake ] )
        # increment the counter for outliers
        n_outlier += 1


    head_tail = os.path.split(output_data_file)
    with open(os.path.join(head_tail[0], 'record_edge_inlier_outlier.txt'), 'w') as fout:
        for ele in inlier_loop_closure_stack:
            fout.write("EDGE2 %d %d inlier\n" % ele)
        for ele in outlier_loop_closure_stack:
            fout.write("EDGE2 %d %d outlier\n" % ele[0])        

    # extract information matrix from an input data file
    info_matrix = ObtainInfoMatrix2D(input_data_file)
    # copy the input_data_file to output_data_file, the existing correct-loop-closures are copied to the output_data_file
    copyfile(input_data_file, output_data_file)
    # write outlier edges to output_data_file
    fout = open(output_data_file, 'a')
    for ele in outlier_loop_closure_stack:
        fout.write("EDGE2 %d %d " % ele[0])
        fout.write("%.6f %.6f %.6f " % ele[1])
        fout.write("%.3f %.3f %.3f %.3f %.3f %.3f\n" % info_matrix)

    fout.close()
    # FINISH
    print ("%d outliers added to the dataset: saved as {}".format(output_data_file) % outlier_num)



# check if a edge given by (v1, v2) exists
def ExistLoopClosure(v1, v2, inlier_loop_closure_stack, outlier_loop_closure_stack):
    # check the existence of edge (v1, v2) in the inlier_loop_closure_stack
    for i in range(0, len(inlier_loop_closure_stack)):
        if(v1 == inlier_loop_closure_stack[i][0] and v2 == inlier_loop_closure_stack[i][1]):
            return True
        elif(v1 == inlier_loop_closure_stack[i][1] and v2 == inlier_loop_closure_stack[i][0]):
            return True
        else:
            continue
    # check the existence of edge (v1, v2) in the outlier_loop_closure_stack
    for i in range(0, len(outlier_loop_closure_stack)):
        if(v1 == outlier_loop_closure_stack[i][0][0] and v2 == outlier_loop_closure_stack[i][0][1]):
            return True
        elif(v1 == outlier_loop_closure_stack[i][0][1] and v2 == outlier_loop_closure_stack[i][0][0]):
            return True
        else:
            continue
    # return 0 if edge(v1, v2) is a completely new edge
    return False


# Check if two measurements are too close
def IsMeasurementClose2D(Measurement1, Measurement2):
    diff = MimickMeasurement2D(Measurement1, Measurement2)
    diff_squaredNorm = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2]
    return (diff_squaredNorm < 10.0)


# Mimick 2D relative pose measurement
def MimickMeasurement2D(V1, V2):
    dx = V2[0] - V1[0]
    dy = V2[1] - V1[1]
    sino1 = sin(V1[2])
    coso1 = cos(V1[2])
    sino2 = sin(V2[2])
    coso2 = cos(V2[2])
    ddx = coso1 * dx - sino1 * dy
    ddy = sino1 * dx + coso1 * dy
    ddocos =  coso1 * coso2 + sino1 * sino2
    ddosin = -sino1 * coso2 + coso1 * sino2
    ddo = atan2(ddosin, ddocos)
    return (ddx, ddy, ddo)


# Return a pair of integers within the interval [lb, ub]
def RandomPair(lb, ub):
    p1 = random.randint(lb, ub)
    p2 = random.randint(lb, ub)
    while (abs(p2 - p1) < 4):
        p2 = random.randint(lb, ub)
    return (p1, p2)


# Obtain information matrix
def ObtainInfoMatrix2D(input_data_file):
    with open(input_data_file) as fdinp:
        for line in fdinp:
            chline = line.split(' ')
            prefix = chline[0]
            if(prefix[0:4]=='EDGE'):
                return ( float(chline[6]), float(chline[7]), float(chline[8]), float(chline[9]), float(chline[10]), float(chline[11]) )



if __name__ == '__main__':
    main()



