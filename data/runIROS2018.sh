#!/bin/bash

# The directory of orginal files
FILES=incremental/*

# The directory for target files

TARGET=Results

BIN_DIR=../bin

INC_LIN=${TARGET}/IncrementalSolver/LinearizeMeasurement
INC_DON=${TARGET}/IncrementalSolver/DontLinearizeMeasurement
BAT_LIN=${TARGET}/BatchSolver/LinearizeMeasurement
BAT_DON=${TARGET}/BatchSolver/DontLinearizeMeasurement

mkdir $INC_LIN -p
mkdir $INC_DON -p
mkdir $BAT_LIN -p
mkdir $BAT_DON -p


for f in $FILES

do
    printf "\nProcess $f\n"

    file_name="${f##*/}"
    extension="${file_name##*.}"
    filename="${file_name%.*}"


    objFunc_file=${INC_LIN}/${filename}_objFuncChange.txt
    vertices_file=${INC_LIN}/${filename}_vertices.txt
    summary_file=${INC_LIN}/${filename}_summary.txt
    if [ ! -f $objFunc_file ] || [ ! -f $vertices_file ] || [ ! -f $summary_file ]; then
    	echo "solve $f : incrementalSolver = true, linearizeMeasurement = true"
	echo "=========================================================="
    	${BIN_DIR}/slam_pose_graph $f -vf $vertices_file -of $objFunc_file -sf $summary_file -is -lm
	echo "=========================================================="
    else
    	echo "solve $f : incrementalSolver = true, linearizeMeasurement = true : Results Files Exists, Skippted!"
    fi


    objFunc_file=${INC_DON}/${filename}_objFuncChange.txt
    vertices_file=${INC_DON}/${filename}_vertices.txt
    summary_file=${INC_DON}/${filename}_summary.txt
    if [ ! -f $objFunc_file ] || [ ! -f $vertices_file ] || [ ! -f $summary_file ]; then
    	echo "solve $f : incrementalSolver = true, linearizeMeasurement = false"
	echo "=========================================================="
    	${BIN_DIR}/slam_pose_graph $f -vf $vertices_file -of $objFunc_file -sf $summary_file -is
	echo "=========================================================="
    else
    	echo "solve $f : incrementalSolver = true, linearizeMeasurement = false : Results Files Exists, Skippted!"
    fi


    objFunc_file=${BAT_LIN}/${filename}_objFuncChange.txt
    vertices_file=${BAT_LIN}/${filename}_vertices.txt
    summary_file=${BAT_LIN}/${filename}_summary.txt
    if [ ! -f $objFunc_file ] || [ ! -f $vertices_file ] || [ ! -f $summary_file ]; then
    	echo "solve $f : incrementalSolver = false, linearizeMeasurement = true"
	echo "=========================================================="
    	${BIN_DIR}/slam_pose_graph $f -vf $vertices_file -of $objFunc_file -sf $summary_file -lm
	echo "=========================================================="
    else
    	echo "solve $f : incrementalSolver = false, linearizeMeasurement = true : Results Files Exists, Skippted!"
    fi


start=$(date +%s.%N)

    objFunc_file=${BAT_DON}/${filename}_objFuncChange.txt
    vertices_file=${BAT_DON}/${filename}_vertices.txt
    summary_file=${BAT_DON}/${filename}_summary.txt
    if [ ! -f $objFunc_file ] || [ ! -f $vertices_file ] || [ ! -f $summary_file ]; then
    	echo "solve $f : incrementalSolver = false, linearizeMeasurement = false"
	echo "=========================================================="
    	${BIN_DIR}/slam_pose_graph $f -vf $vertices_file -of $objFunc_file -sf $summary_file 
	echo "=========================================================="
    else
    	echo "solve $f : incrementalSolver = false, linearizeMeasurement = false : Results Files Exists, Skippted!"
    fi

end=$(date +%s.%N)
runtime=$( echo "$end - $start" | bc -l )
echo "$Finish {file_name}: used_time ${runtime}"


done

echo "Finished!"

lscpu > ${TARGET}/CPU_Info.txt


