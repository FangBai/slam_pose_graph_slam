A brief overview of the python/bash files used for the IROS2018 paper:

[Predicting Objective Function Change in Pose-Graph Optimization](https://opus.lib.uts.edu.au/bitstream/10453/128555/4/IROS2018-FangBai.pdf)

by Fang Bai1, Teresa Vidal-Calleja, Shoudong Huang, and Rong Xiong



---
**sortDataIncremental.py**

A python script is used to sort the pose graph data in an incremental sequence mimicking the real-time loop-closure sequence


---
**addOutliers.py**

A python script used to add outliers to a dataset



---
**bashsort.sh**


A bash file calling "sortDataIncremental.py" for each file in the directory "dataset", the result is saved in the directory "incremental"



---
**runIROS2018.sh**


A bash file to calculating the "change of optimal values" (i.e. prediction of the objective function change) for each dataset in the folder "incremental", the result is saved in the directory "Result"



---
**INTEL**

An illustration of outlier detection with the INTEL dataset. Inside the "INTEL" directory:

**runMonteCarlo.sh**: a bash file to run Monte Carlo simulation on the INTEL dataset, in terms of different number of outliers. The file tests two methods: i.e., rejecting outliers by the change of optimal values, VS incremental M-estimators.

---
Below three files uses the input "./data_dir/INTEL_outlier_sorted.txt" for a single run.

- **INTEL/runINTEL.sh**: a bash file to remove outleirs by the change of optimal values
- **INTEL/runRobustBatch.sh**: a bash file to remove outliers by M-estimators, processing measurements in batch
- **INTEL/runRobustOnline.sh**: a bash file to remove outliers by M-estimators, processing measurements incrementally 
