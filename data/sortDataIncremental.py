#!/usr/bin/python

# This script is used to sort the pose graph data in an incremental sequence mimicking the real-time loop-closure sequence
# namely, arrange sequence <0,1> <1,2> <1,3> <2, 3> to sequence <0,1> <1,2> <2,3> <1,3>.
# use the script by the syntax:
# ------------------------------------------- #
# python sortDataIncremental.py <inputfile> <outputfile>
# ------------------------------------------- #
# Fang Bai @ Jan-5-2018
# University of Technology Sydney
# Fang.Bai@student.uts.edu.au


import sys, os


def main():

    if len(sys.argv) < 3:
        print ("Please use the script in the format:\n")
        print ("python <script_name> <input_filename> <output_filename>\n")
        sys.exit()

    inputfile = sys.argv[1]
    outputfile = sys.argv[2]

    if not os.path.isfile(inputfile):
        print ("Input file {} does not exist.".format(inputfile))
        sys.exit()

    fout = open(outputfile, 'w')
    exist_id = 0
    wait_id_stack = list()

    
    odometry_stack = list()
    loop_closure_stack = list()

    # get odometry and loop-closures
    with open(inputfile) as finp:
        for line in finp:
            prefix, sn, tn = obtainPrefixNodeIDs(line)
            if isVertexPrefix(prefix): 
                # fout.write(line)
                continue
            elif isOdometry(sn, tn):
                if(min(sn,tn) == exist_id):
                    exist_id += 1
                    odometry_stack.append(line)
                else:
                    print ("Error: Odometry data is not in sequence!")
                    sys.exit()
            else:
                wait_id = max(sn, tn)
                lpcl_stack_index = len(loop_closure_stack)
                wait_id_stack.append([wait_id, lpcl_stack_index])
                loop_closure_stack.append(line)
    

    # loop odometry, check loop-closures related to the new vertex
    exist_id = 0
    for odom in odometry_stack:
        fout.write(odom)
        exist_id += 1
        ready_list, wait_id_stack = checkWaitIdStack (wait_id_stack, exist_id)
        for i in ready_list:
            fout.write(loop_closure_stack[i])


    fout.close()
    print ("edge sequence reorganized in online sequence. saved as {}".format(outputfile))


def checkWaitIdStack(wait_id_stack, exist_id):
    ready_list = list()
    wait_id_del = list()
    for i in range(0, len(wait_id_stack)):
        if (wait_id_stack[i][0] <= exist_id):
            ready_list.append(wait_id_stack[i][1])
            wait_id_del.append(i)
    for i in sorted(wait_id_del, reverse=True):
        del wait_id_stack[i]
    return (ready_list, wait_id_stack)


def obtainPrefixNodeIDs(line):
    chline = line.split(' ')
    prefix = chline[0]
    if isVertexPrefix(prefix):
        node1 = -1
        node2 = -1
    else:
        node1 = int(chline[1])
        node2 = int(chline[2])
    return(prefix, node1, node2)


def isVertexPrefix(prefix):
    return (prefix[0:6]=='VERTEX')


def isOdometry(node1, node2):
    return (node2 - node1 == 1 or node1 - node2 == 1)


if __name__ == '__main__':
    main()




