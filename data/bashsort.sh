#!/bin/bash

# The directory of orginal files
FILES=dataset/*

# The directory for target files
TARGET=incremental

mkdir $TARGET

for f in $FILES

do
    echo "Process $f"

    file_name="${f##*/}"

    extension="${file_name##*.}"

    filename="${file_name%.*}"
   
    target_file=${TARGET}/${filename}_incremental.${extension}

    python sortDataIncremental.py $f $target_file

done

echo "Finished!"
