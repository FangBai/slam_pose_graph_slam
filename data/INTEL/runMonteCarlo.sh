#!/bin/bash
declare -a NumOutlier=( 250 200 150 100 75 50 25 10 5 0 )
# declare -a NumOutlier=( 0 5 10 25 50 75 100 150 200 250 )
MC_TotalNum=20

results_dir=./MonteCarlo_dir


bin_pofc=../../bin/slam_pose_graph
bin_robust=../../bin/slam_robust_pgo
file_data=../dataset/input_INTEL_g2o.g2o
file_solution=../Results/IncrementalSolver/DontLinearizeMeasurement/input_INTEL_g2o_incremental_vertices.txt


for n in ${NumOutlier[@]}; do

    for i in $(seq 1 $MC_TotalNum); do

        file_dir=${results_dir}/Outlier_${n}/Trial_${i}

        mkdir ${file_dir} -p

        file_output_unsorted=${file_dir}/INTEL_outlier_unsorted.txt
        file_output_sorted=${file_dir}/INTEL_outlier_sorted.txt

        python ../addOutliers.py ${file_data} ${file_solution} ${file_output_unsorted} $n
        python ../sortDataIncremental.py ${file_output_unsorted} ${file_output_sorted}

        data_file=$file_output_sorted

        pofc_solution_file=${file_dir}/pofc_vertices_outlier.txt
        pofc_objfunc_file=${file_dir}/pofc_objFunc_outlier.txt
        pofc_summary_file=${file_dir}/pofc_summary_outlier.txt

        robust_solution_file=${file_dir}/robust_vertices_outlier.txt
        robust_objfunc_file=${file_dir}/robust_objFunc_outlier.txt
        robust_summary_file=${file_dir}/robust_summary_outlier.txt

        # run diiferent approaches
        ${bin_pofc} ${data_file} -vf ${pofc_solution_file} -of ${pofc_objfunc_file} -sf ${pofc_summary_file}
        ${bin_robust} ${data_file} -vf ${robust_solution_file} -of ${robust_objfunc_file} -sf ${robust_summary_file} -is

        echo "finished outlier_num = $n, MC_count = $i"

    done

    python processMC_Results.py ${n} ${MC_TotalNum} ${results_dir}/Timing_${n}.txt ${results_dir}/error_${n}.txt



done



