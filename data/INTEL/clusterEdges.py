#!/usr/bin/python

# This script is used to cluster the edges according to "edge_status" flag.
# 0 - accepted;   1 - pending;   2 - rejected
# ------------------------------------------- #
# python <script_name> <input_filename>
# ------------------------------------------- #
# Fang Bai @ Feb-9-2018
# University of Technology Sydney
# Fang.Bai@student.uts.edu.au


import sys, os

def main():

    if len(sys.argv) < 2:
        print ("Please use the script in the format:\n")
        print ("python <script_name> <input_filename>\n")
        sys.exit()

    inputfile = sys.argv[1]

    if not os.path.isfile(inputfile):
        print ("Input file {} does not exist.".format(inputfile))
        sys.exit()

    filename, file_extension = os.path.splitext(inputfile)

    fout0 = open(filename+"_accepted"+file_extension, 'w')
    fout1 = open(filename+"_pending"+file_extension, 'w')
    fout2 = open(filename+"_rejected"+file_extension, 'w')

    with open(inputfile) as finp:
        for line in finp:
            edge_status = int(line.split(' ')[3])
            if(edge_status == 0):
                fout0.write(line)
            elif(edge_status == 1):
                fout1.write(line)
            elif(edge_status == 2):
                fout2.write(line)
            else:
                print ("flag error.\n")
                sys.exit()

    # close the files
    fout0.close()
    fout1.close()
    fout2.close()

    print ("The edges have been successfully separated according status. Written in separte files.\n")



if __name__ == '__main__':
    main()


