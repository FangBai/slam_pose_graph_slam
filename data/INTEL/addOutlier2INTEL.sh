#!/bin/bash

file_data=../dataset/input_INTEL_g2o.g2o
file_solution=../Results/IncrementalSolver/DontLinearizeMeasurement/input_INTEL_g2o_incremental_vertices.txt

data_dir=./data_dir
mkdir ${data_dir}

file_output_unsorted=${data_dir}/INTEL_outlier_unsorted.txt
file_output_sorted=${data_dir}/INTEL_outlier_sorted.txt

num_outlier=20
if [ $# -eq 1 ]; then
    num_outlier=$1
fi


python ../addOutliers.py ${file_data} ${file_solution} ${file_output_unsorted} ${num_outlier}

python ../sortDataIncremental.py ${file_output_unsorted} ${file_output_sorted}


