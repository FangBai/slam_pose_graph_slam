#!/bin/bash

start=$(date +%s.%N)

results_dir=./robust_results_dir

mkdir ${results_dir}

# data file
data_file=./data_dir/INTEL_outlier_sorted.txt

# solution file
solution_file=${results_dir}/INTEL_vertices_outlier.txt

# objective function change file
objfunc_file=${results_dir}/INTEL_objFunc_outlier.txt

# summary file
summary_file=${results_dir}/INTEL_summary_outlier.txt

# bin file
bin_file=../../bin/slam_robust_pgo

${bin_file} ${data_file} -vf ${solution_file} -of ${objfunc_file} -sf ${summary_file} -is


# python clusterEdges.py $objfunc_file


end=$(date +%s.%N)
runtime=$( echo "$end - $start" | bc -l )

echo "FINISHED: used_time ${runtime}"
