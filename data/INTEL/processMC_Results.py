#!/usr/bin/python

# This script is used to cluster the edges according to "edge_status" flag.
# 0 - accepted;   1 - pending;   2 - rejected
# ------------------------------------------- #
# python <script_name> <input_filename>
# ------------------------------------------- #
# Fang Bai @ Feb-9-2018
# University of Technology Sydney
# Fang.Bai@student.uts.edu.au


import sys, os
import math


def main():

    
    MC_dir = "MonteCarlo_dir"


    benchmark_vertices_file = "benchmark/benchmark_vertices.txt"


    Outlier_num = int(sys.argv[1])
    MC_num = int(sys.argv[2])
    timing_output_file = sys.argv[3]
    error_output_file = sys.argv[4]


    Timing_stack = list()
    Error_stack = list()

    for i in range(1, MC_num+1):

        f_dir = MC_dir + '/' + 'Outlier_' + str(Outlier_num) + '/' + 'Trial_' + str(i) + '/'



        pofc_summary_file = f_dir + "pofc_summary_outlier.txt"
        robust_summary_file = f_dir + "robust_summary_outlier.txt"
        pofc_vertices_file = f_dir + "pofc_vertices_outlier.txt"
        robust_vertices_file = f_dir + "robust_vertices_outlier.txt"

        pofc_timing = GetTiming(pofc_summary_file)
        robust_timing = GetTiming(robust_summary_file)
        pofc_error = GetATE (pofc_vertices_file, benchmark_vertices_file)
        robust_error = GetATE (robust_vertices_file, benchmark_vertices_file)

        Timing_stack.append( (pofc_timing, robust_timing) )
        Error_stack.append( (pofc_error, robust_error) )

    ft = open(timing_output_file, 'w')
    for ele in Timing_stack:
        ft.write("%f %f\n" % ele)
    ft.close()

    fe = open(error_output_file, 'w')
    for ele in Error_stack:
        fe.write("%f %f\n" % ele)
    fe.close()





def GetTiming(filename):
    with open(filename) as f:
        for line in f:
            chline = line.split(' ')
            if (chline[0] == 'TotalTiming' and chline[1] == '='):
                return float(chline[2])



def GetATE(solution_file, benchmark_file):
    tra_solution = list()
    tra_benchmark = list()
    with open(solution_file) as fs:
        for line in fs:
            chline = line.split(' ')
            tra_solution.append ( ( float(chline[2]), float(chline[3]), float(chline[4]) ) )
    with open(benchmark_file) as fb:
        for line in fb:
            chline = line.split(' ')
            tra_benchmark.append ( ( float(chline[2]), float(chline[3]), float(chline[4]) ) )
    return ATE(tra_solution, tra_benchmark)



def ATE(Trajectory1, Trajectory2):
    if (len(Trajectory1) != len(Trajectory2)):
        print ("Error: trajectory1 and trajectory2 have different number of poses\n")
        sys.exit()
    ATE_error = .0
    for i in range(0, len(Trajectory1)):
        pose1 = Trajectory1[i]
        pose2 = Trajectory2[i]
        dx = pose1[0] - pose2[0]
        dy = pose1[1] - pose2[1]
        ATE_error += dx * dx + dy * dy
    return math.sqrt(ATE_error)



if __name__ == '__main__':
    main()




