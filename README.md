    Application of the Change of Optimal Values - Outlier Detection on Pose Graph Optimization (PGO)
    

Maintainer: Fang Bai (fang.bai@yahoo.com, fang.bai@alumni.uts.edu.au, fang.bai@student.uts.edu.au)

** The repository contains the code for the paper: [Predicting Objective Function Change in Pose-Graph Optimization](https://opus.lib.uts.edu.au/bitstream/10453/128555/4/IROS2018-FangBai.pdf), Fang Bai et al. IROS2018 **

---
### Directory of PGO projects ###

 - [`src/slam_pose_graph/`](./src/slam_pose_graph/) implements the equation to predict the *change of optimal values*

 - [`src/slam_robust_pgo/`](./src/slam_robust_pgo/) implements M-estimators
 

---
### Installation requirement ###

  The code should compile as long as SLAM++ (https://sourceforge.net/p/slam-plus-plus/wiki/Home/) compiles.


---
### How to run the code ###

 - Download the code

        git clone git@bitbucket.org:FangBai/slam_pose_graph_slam.git

 - Compile the code
 
        mkdir build -p && cd build && cmake .. && make && cd ..
           
 - Run **slam_pose_graph** by
   
        ./bin/slam_pose_graph
	
    to check the prompts on how to run the code.
   	
 - Run **slam_robust_pgo** by
           
        ./bin/slam_robust_pgo
	
    to check the prompts on how to run the code.


---
### Format of the input file ###

 Please see the format of the files in the directory [`data/dataset/`](./data/dataset/) and [`data/incremental/`](./data/incremental/) 
 
 **Notice** To use the closed-form equation to predict change of optimzal values, make sure the edges are sorted such that the odometry to a vertex is added before related loop-closures. For example, we require odometry Edge(4,5) to be added before a loop-closure like Edge(2,5). Thus the marginal covariance of vertex 2 and 5 exists when evaluating the loop-closure Edge(2,5).
 
 The data with sorted edges are in the directory [`data/incremental/`](./data/incremental/).
 
 - 2D PGO format:

        EDGE2 vID1 vID2 px py o Info11 Info12 Info13 Info22 Info23 Info33
       
        EDGE_SE2 vID1 vID2 px py o Info11 Info12 Info13 Info22 Info23 Info33  
       
 - 3D PGO format:
 
        EDGE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66
	
        EDGE_SE3 vID1 vID2 px py pz o1 o2 o3 Info11 Info12 Info13 Info14 Info15 Info16 Info22 Info23 Info24 Info25 Info26 Info33 Info34 Info35 Info36 Info44 Info45 Info46 Info55 Info56 Info66         


---
### Scripts for IROS2018 paper ###

**The scripts (bash and python files) are placed in the directory [**data/**](./data/).**

Some details are provided in [**data/README.md**](./data/README.md) to help reproduce the results.

**Notice** To check the difference between the *actual/real objective function change** vs the *predicted objective function change*. Please use the branch **IROS2018** by running:

        git checkout IROS2018
        
Then compile again by

        mkdir build -p && cd build && cmake .. && make && cd ..

and run the data inside [`data/incremental/`](./data/incremental/), for example:

        ./bin/slam_pose_graph data/incremental/intel_incremental.txt -of outputfile.txt        

The statistics are reported in the outputfile, with the format (per line):

        index  vertex0  vertex1  0  predictedObjectiveFunctionChange  actualObjectiveFunctionChange  objectiveFunctionChangeAbsoluteError  objectiveFunctionChangeRelativeError(FromPredictedToActual)  objectiveFunctionChangeRelativeError(FromActualToPredicted)  usedIterations


---
### Related publication ###

```
@inproceedings{bai2018predicting,
  title={Predicting Objective Function Change in Pose-Graph Optimization},
  author={Bai, Fang and Vidal-Calleja, Teresa and Huang, Shoudong and Xiong, Rong},
  booktitle={2018 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)},
  pages={145--152},
  year={2018},
  organization={IEEE}
}
```
