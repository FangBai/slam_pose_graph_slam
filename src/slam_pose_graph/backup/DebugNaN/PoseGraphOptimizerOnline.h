//
// Created by fang on 20/01/18.
//

#ifndef SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H
#define SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H



#include <stdio.h> // printf
#include <algorithm>
#include <numeric>
#include <queue>
#ifdef _OPENMP
#include <omp.h>
#endif // _OPENMP
#include <vector>
// #include <list>

#include "PoseGraphOptimizer.h"



template <const bool b_2D_SLAM = true, const bool b_Incremental_Solver = false>
class CPoseGraphOptimizerOnline : public CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver> {

public:

    typedef typename CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::_TyMatrix  _TyMatrix; /**< @copydoc _TyEdgeData::_TyMatrix */
    typedef typename CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::_TyVector _TyVector; /**< @copydoc _TyEdgeData::_TyVector */

    typedef Eigen::Matrix<double, (b_2D_SLAM)? 3 : 4, (b_2D_SLAM)? 3 : 4> _TyPoseMatrix;

    typedef typename LieGroup<b_2D_SLAM>::_TySE SE;  // Lie Group SE Operations


    /**
     *	@brief decision type of edges stored as enum
     */
    enum EdgeStatus{
        status_accepted = 0, /**< @brief accepted as an inlier */
        status_pending = 1, /**< @brief cannot decide yet, decision pending */
        status_rejected = 2 /**< @brief rejected as an outlier */
    };

    struct TEdgeInfo; // forward declaration for edge information type


    CPoseGraphOptimizerOnline(bool b_linearize_measurement, size_t n_max_iteration_num = 5, double f_error_threshold = .001)
            :CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>(n_max_iteration_num, f_error_threshold),
             m_b_linearize_measurement(b_linearize_measurement)
    {
        m_EdgeInfoRecord = std::vector<TEdgeInfo>();
        m_EdgeInfoRecord.clear();
    }

    virtual ~CPoseGraphOptimizerOnline() {}


    int FeedDataOnline(size_t n_vertex_id1, size_t n_vertex_id2, const _TyVector &v_measurement, const _TyMatrix &t_information)
    {
        printf("Process edge <%d, %d> \n", (int) n_vertex_id1, (int) n_vertex_id2);
        double delta_objFunc_predicted = .0; //predicted objFunc change
        EdgeStatus e_edge_status = status_accepted;
        if( !(n_vertex_id2 - n_vertex_id1 == 1 || n_vertex_id1 - n_vertex_id2 == 1) ){ /* loop-closure edges */
            delta_objFunc_predicted = this->PredictObjFuncChange(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
            e_edge_status = EdgeDecision(delta_objFunc_predicted); // Edge decision according to objFunc change
        }
        double pre_objFunc = this->Get_ObjFunc(); // previous objFunc before optimization
        // objFunc in first 3 iterations
        double objFuncIter0 = pre_objFunc;
        double objFuncIter1 = pre_objFunc;
        double objFuncIter2 = pre_objFunc;
        double objFuncIter3 = pre_objFunc;
        // different operations according to EdgeStatus
        if (e_edge_status == status_accepted) {
            if(b_Incremental_Solver){
                // using incremental solver
                this->Incremental_Step(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
                this->Optimize(1, .001); // allow one iteration to increase accuracy
                // observations: zero iteration is not accurate because the solver does not update
                // only one iteration is pretty good for online solver, usually converge well
            }else{
                // using batch solver
                this->Add_Edge(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
          //      objFuncIter0 = this->Get_ObjFunc();  // objFunc Iter0
                // optimize 3 times and recording the objFuncs
                this->Optimize(1, .001);
                objFuncIter1 = this->Get_ObjFunc();  // objFunc Iter1
                this->Optimize(1, .001);
                objFuncIter2 = this->Get_ObjFunc();  // objFunc Iter2
                this->Optimize(1, .001);
                objFuncIter3 = this->Get_ObjFunc();  // objFunc Iter3
                this->Optimize(5, .001); // continue optimize until convergence
            }
        } else if (e_edge_status == status_rejected) {
            // Add to the rejected list, do not optimize
        } else if (e_edge_status == status_pending) {
            // Add to the pending list, do not optimize
        }
        double cur_objFunc = this->Get_ObjFunc(); // current objFunc after optimization
        double delta_objFunc_real = cur_objFunc - pre_objFunc; // real objFunc change
        if( !(n_vertex_id2 - n_vertex_id1 == 1 || n_vertex_id1 - n_vertex_id2 == 1) )
        {
            TEdgeInfo edge_info;
            edge_info.vertex[0] = n_vertex_id1;
            edge_info.vertex[1] = n_vertex_id2;
            edge_info.status = e_edge_status;
            edge_info.objFuncChange_predicted = delta_objFunc_predicted;
            edge_info.objFuncChange_real = delta_objFunc_real;
            edge_info.objFuncChange_absError = delta_objFunc_predicted - delta_objFunc_real;
            edge_info.objFuncChange_rvlError = delta_objFunc_predicted/delta_objFunc_real - 1;
            edge_info.objFuncChange_Iter1 = objFuncIter1 - pre_objFunc;
            edge_info.objFuncChange_Iter2 = objFuncIter2 - objFuncIter1;
            edge_info.objFuncChange_Iter3 = objFuncIter3 - objFuncIter2;
            edge_info.objFuncChange_Portion_Iter1 = (objFuncIter1 - pre_objFunc)/delta_objFunc_real;
            m_EdgeInfoRecord.push_back(edge_info);
        }
        return e_edge_status;
    }


    double PredictObjFuncChange(size_t n_vertex_id1, size_t n_vertex_id2, const _TyVector &v_measurement, const _TyMatrix &t_information)
    {
        // get pose estimates
        _TyVector v_pose1 = this->Get_Vertex(n_vertex_id1);
        _TyVector v_pose2 = this->Get_Vertex(n_vertex_id2);
        // get covariance of the estimates
        _TyMatrix Sigma11 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id1);
        _TyMatrix Sigma12 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id2);
        _TyMatrix Sigma22 = this->Get_CovarianceBlock(n_vertex_id2, n_vertex_id2);
        // compute predicted objFunc change
        double obj_func_change = .0;
        //
        // matrix form of poses 3by3 or 4by4
        _TyPoseMatrix inv_mst = SE::Inv(SE::V2M(v_measurement));
        _TyPoseMatrix inv_pose1 = SE::Inv(SE::V2M(v_pose1));
        _TyPoseMatrix pose2 = SE::V2M(v_pose2);
        // intermediate vectors and matrices
        _TyVector mst_error;
        _TyMatrix J1, J2, J3, mst_error_cov;
        // measurement error
        mst_error = SE::Log(inv_mst * inv_pose1 * pose2);
        // J = [ J1, J2, J3]
        J1 = SE::InvJl(mst_error) * SE::Ad(inv_mst);
        J2 = -SE::InvJr(mst_error);
        if(m_b_linearize_measurement){
            J3 = SE::InvJl(mst_error);
        }else{
            J3 = _TyMatrix::Identity();
        }
        // measurement error covariance expansion
        mst_error_cov = J1 * Sigma11 * J1.transpose()
                        + J1 * Sigma12 * J2.transpose()
                        + J2 * Sigma12.transpose() * J1.transpose()
                        + J2 * Sigma22 * J2.transpose()
                        + J3 * t_information.inverse() * J3.transpose();
        // predicted objective function change
        obj_func_change = mst_error.dot (mst_error_cov.inverse() * mst_error);
        return obj_func_change;
    }


    EdgeStatus EdgeDecision(double delta_obj_func) {
        // outlire free cases
        return status_accepted; // accept the edge no matter how large the objFuncChange is
    }


    void SaveEdgeInfoRecord(const char *p_s_filename)
    {
        std::FILE * record_file;
        record_file = std::fopen(p_s_filename, "w");
        for (size_t i = 0, n = m_EdgeInfoRecord.size(); i < n; ++ i)
        {
            TEdgeInfo & edge_info = m_EdgeInfoRecord[i];
            std::fprintf(record_file, "%zu %zu %d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n",
                         edge_info.vertex[0], edge_info.vertex[1], edge_info.status,
                         edge_info.objFuncChange_predicted, edge_info.objFuncChange_real,
                         edge_info.objFuncChange_absError, edge_info.objFuncChange_rvlError,
                         edge_info.objFuncChange_Iter1, edge_info.objFuncChange_Iter2,
                         edge_info.objFuncChange_Iter3, edge_info.objFuncChange_Portion_Iter1
            );
        }
        std::fclose(record_file);
        printf("Edge information has been saved into the file: \"%s\" \n", p_s_filename);
    }


private:

    bool m_b_linearize_measurement;  // Either is good!!!

    std::vector<CPoseGraphOptimizerOnline::TEdgeInfo> m_EdgeInfoRecord;

};


// structure to preserve run time information regarding objFuncChanges
template <const bool b_2D_SLAM, const bool b_Incremental_Solver>
struct CPoseGraphOptimizerOnline<b_2D_SLAM, b_Incremental_Solver>::TEdgeInfo
{
    size_t vertex[2];
    CPoseGraphOptimizerOnline::EdgeStatus status;
    double objFuncChange_predicted;
    double objFuncChange_real;
    double objFuncChange_absError;
    double objFuncChange_rvlError;
    double objFuncChange_Iter1;
    double objFuncChange_Iter2;
    double objFuncChange_Iter3;
    double objFuncChange_Portion_Iter1;
};



#endif //SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H
