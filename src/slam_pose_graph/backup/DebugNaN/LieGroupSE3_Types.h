/**
 *	@file include/slam/SE3_Types.h
 *	@author Soso
 *	@date 2013
 *	@brief projection type for SE3
 */

#pragma once
#ifndef __SE3_TYPES_INCLUDED
#define __SE3_TYPES_INCLUDED

#include "slam/BaseTypes.h"

#include "slam/Parser.h" // parsed types passed to constructors

#include "LieGroup.h" /* include Lie Group implementation */

#include <iostream>

/** \addtogroup se3
 *	@{
 */

/**
 *	@brief SE(3) pose vertex type
 */
class CVertexPose3D : public CBaseVertexImpl<CVertexPose3D, 6> {
public:
	__GRAPH_TYPES_ALIGN_OPERATOR_NEW

	/**
	 *	@brief default constructor; has no effect
	 */
	inline CVertexPose3D()
	{}

	/**
	 *	@brief constructor; initializes state vector
	 *	@param[in] r_v_state is state vector initializer
	 */
	inline CVertexPose3D(const Eigen::Matrix<double, 6, 1> &r_v_state)
		:CBaseVertexImpl<CVertexPose3D, 6>(r_v_state)
	{}

	/**
	 *	@copydoc base_iface::CVertexFacade::Operator_Plus()
	 */
	inline void Operator_Plus(const Eigen::VectorXd &r_v_delta) // "smart" plus
	{
        Eigen::Vector6d cv_delta = r_v_delta.segment<6>(m_n_order);
        Eigen::Matrix4d Tmatr = SE3::V2M(m_v_state); // transform vector storage to matrix storage
        Tmatr = Tmatr * SE3::Exp(cv_delta); // update with exponential mapping
        m_v_state = SE3::M2V(Tmatr);  // transform matrix storage to vector storage
	}

	/**
	 *	@copydoc base_iface::CVertexFacade::Operator_Minus()
	 */
	inline void Operator_Minus(const Eigen::VectorXd &r_v_delta) // "smart" minus
	{
        Eigen::Vector6d cv_delta = r_v_delta.segment<6>(m_n_order);
        Eigen::Matrix4d Tmatr = SE3::V2M(m_v_state); // transform vector storage to matrix storage
        Tmatr = Tmatr * SE3::Exp(-cv_delta); // update with exponential mapping
        m_v_state = SE3::M2V(Tmatr);  // transform matrix storage to vector storage
    }
};


/**
 *	@brief SE(3) pose-pose edge
 */
class CEdgePose3D : public CBaseEdgeImpl<CEdgePose3D, MakeTypelist(CVertexPose3D, CVertexPose3D), 6> {
public:
	/**
	 *	@brief vertex initialization functor
	 *	Calculates vertex position from the first vertex and an XYT edge.
	 */
	class CRelative_to_Absolute_XYZ_Initializer {
	protected:
		const Eigen::Matrix<double, 6, 1> &m_r_v_pose1; /**< @brief the first vertex */
		const Eigen::Matrix<double, 6, 1> &m_r_v_delta; /**< @brief the edge, shared by r_v_vertex1 and the vertex being initialized */

	public:
		/**
		 *	@brief default constructor
		 *
		 *	@param[in] r_v_vertex1 is the first vertex
		 *	@param[in] r_v_delta is the edge, shared by r_v_vertex1 and the vertex being initialized
		 */
		inline CRelative_to_Absolute_XYZ_Initializer(const Eigen::Matrix<double, 6, 1> &r_v_vertex1,
			const Eigen::Matrix<double, 6, 1> &r_v_delta) // just change the types, same as above
			:m_r_v_pose1(r_v_vertex1), m_r_v_delta(r_v_delta)
		{}

		/**
		 *	@brief function operator
		 *	@return Returns the value of the vertex being initialized.
		 */
		inline operator CVertexPose3D() const // this function calculates initial prior from the state of the first vertex m_r_v_pose1 and from the edge measurement m_r_edge
		{
            Eigen::Matrix4d Pose2 = SE3::V2M(m_r_v_pose1) * SE3::V2M(m_r_v_delta);
            // initialized by compounding first pose and the edge
            return CVertexPose3D(SE3::M2V(Pose2));
		}
	};

public:
	__GRAPH_TYPES_ALIGN_OPERATOR_NEW // imposed by the use of eigen, just copy this

	/**
	 *	@brief default constructor; has no effect
	 */
	inline CEdgePose3D()
	{}

	/**
	 *	@brief constructor; converts parsed edge to edge representation
	 *
	 *	@tparam CSystem is type of system where this edge is being stored
	 *
	 *	@param[in] r_t_edge is parsed edge
	 *	@param[in,out] r_system is reference to system (used to query edge vertices)
	 */
	template <class CSystem>
	CEdgePose3D(const CParserBase::TEdge3D &r_t_edge, CSystem &r_system)
		:CBaseEdgeImpl<CEdgePose3D, MakeTypelist(CVertexPose3D, CVertexPose3D), 6>(r_t_edge.m_n_node_0,
		r_t_edge.m_n_node_1, r_t_edge.m_v_delta, r_t_edge.m_t_inv_sigma)
	{
		//fprintf(stderr, "%f %f %f\n", r_t_edge.m_v_delta(0), r_t_edge.m_v_delta(1), r_t_edge.m_v_delta(2));

		_ASSERTE(r_t_edge.m_n_node_0 < r_t_edge.m_n_node_1 || // either the vertices are ordered
			(r_t_edge.m_n_node_0 > r_t_edge.m_n_node_1 &&
			r_system.n_Vertex_Num() > r_t_edge.m_n_node_0)); // or they are not, but then both need to be in the system (a reversed loop closure)

		m_p_vertex0 = &r_system.template r_Get_Vertex<CVertexPose3D>(r_t_edge.m_n_node_0, CInitializeNullVertex<>());
		m_p_vertex1 = &r_system.template r_Get_Vertex<CVertexPose3D>(r_t_edge.m_n_node_1,
			CRelative_to_Absolute_XYZ_Initializer(m_p_vertex0->r_v_State(), r_t_edge.m_v_delta));
		// get vertices (initialize if required)

		//_ASSERTE(r_system.r_Vertex_Pool()[r_t_edge.m_n_node_0].n_Dimension() == 6); // get the vertices from the vertex pool to ensure a correct type is used, do not use m_p_vertex0 / m_p_vertex1 for this
		//_ASSERTE(r_system.r_Vertex_Pool()[r_t_edge.m_n_node_1].n_Dimension() == 6);
		// make sure the dimensionality is correct (might not be)
		// this fails with const vertices, for obvious reasons. with the thunk tables this can be safely removed.
	}

	/**
	 *	@brief constructor; converts parsed edge to edge representation
	 *
	 *	@tparam CSystem is type of system where this edge is being stored
	 *
	 *	@param[in] n_node0 is (zero-based) index of the first (origin) node
	 *	@param[in] n_node1 is (zero-based) index of the second (endpoint) node
	 *	@param[in] r_v_delta is measurement vector
	 *	@param[in] r_t_inv_sigma is the information matrix
	 *	@param[in,out] r_system is reference to system (used to query edge vertices)
	 */
	template <class CSystem>
	CEdgePose3D(size_t n_node0, size_t n_node1, const Eigen::Matrix<double, 6, 1> &r_v_delta,
		const Eigen::Matrix<double, 6, 6> &r_t_inv_sigma, CSystem &r_system)
		:CBaseEdgeImpl<CEdgePose3D, MakeTypelist(CVertexPose3D, CVertexPose3D), 6>(n_node0,
		n_node1, r_v_delta, r_t_inv_sigma)
	{
		//fprintf(stderr, "%f %f %f\n", r_t_edge.m_v_delta(0), r_t_edge.m_v_delta(1), r_t_edge.m_v_delta(2));

		_ASSERTE(n_node0 < n_node1 || // either the vertices are ordered
			(n_node0 > n_node1 && r_system.n_Vertex_Num() > n_node0)); // or they are not, but then both need to be in the system (a reversed loop closure)

		m_p_vertex0 = &r_system.template r_Get_Vertex<CVertexPose3D>(n_node0, CInitializeNullVertex<>());
		m_p_vertex1 = &r_system.template r_Get_Vertex<CVertexPose3D>(n_node1,
			CRelative_to_Absolute_XYZ_Initializer(m_p_vertex0->r_v_State(), r_v_delta));
		// get vertices (initialize if required)

		//_ASSERTE(r_system.r_Vertex_Pool()[n_node0].n_Dimension() == 6); // get the vertices from the vertex pool to ensure a correct type is used, do not use m_p_vertex0 / m_p_vertex1 for this
		//_ASSERTE(r_system.r_Vertex_Pool()[n_node1].n_Dimension() == 6);
		// make sure the dimensionality is correct (might not be)
		// this fails with const vertices, for obvious reasons. with the thunk tables this can be safely removed.
	}

	/**
	 *	@brief updates the edge with a new measurement
	 *	@param[in] r_t_edge is parsed edge
	 */
	inline void Update(const CParserBase::TEdge3D &r_t_edge)
	{
		CBaseEdgeImpl<CEdgePose3D, MakeTypelist(CVertexPose3D, CVertexPose3D),
			6>::Update(r_t_edge.m_v_delta, r_t_edge.m_t_inv_sigma);
	}

	/**
	 *	@brief updates the edge with a new measurement
	 *
	 *	@param[in] r_v_delta is new measurement vector
	 *	@param[in] r_t_inv_sigma is new information matrix
	 */
	inline void Update(const Eigen::Matrix<double, 6, 1> &r_v_delta, const Eigen::Matrix<double, 6, 6> &r_t_inv_sigma) // for some reason this needs to be here, although the base already implements this
	{
		CBaseEdgeImpl<CEdgePose3D, MakeTypelist(CVertexPose3D, CVertexPose3D),
			6>::Update(r_v_delta, r_t_inv_sigma);
	}

	/**
	 *	@brief calculates jacobians, expectation and error
	 *
	 *	@param[out] r_t_jacobian0 is jacobian, associated with the first vertex
	 *	@param[out] r_t_jacobian1 is jacobian, associated with the second vertex
	 *	@param[out] r_v_expectation is expecation vector
	 *	@param[out] r_v_error is error vector
	 */
	inline void Calculate_Jacobians_Expectation_Error(Eigen::Matrix<double, 6, 6> &r_t_jacobian0,
		Eigen::Matrix<double, 6, 6> &r_t_jacobian1, Eigen::Matrix<double, 6, 1> &r_v_expectation,
		Eigen::Matrix<double, 6, 1> &r_v_error) const // change dimensionality of eigen types, if required
	{
        // get matrix form of measurements and poses
        Eigen::Matrix4d inv_mst = SE3::Inv(SE3::V2M(m_v_measurement));  /* inv(measurement) */
        Eigen::Matrix4d inv_pose0 = SE3::Inv(SE3::V2M(m_p_vertex0->r_v_State())); /* inv(pose0) */
        Eigen::Matrix4d pose1 = SE3::V2M(m_p_vertex1->r_v_State()); /* pose1 */
        // compute r_v_expectation
        r_v_expectation = SE3::M2V(inv_pose0 * pose1); // why need r_v_expectation, for what?
        // compute linearization Jacobians and error
        // Linear System || Jacobian * x - error ||
        Eigen::Matrix<double, 6, 1> v_error = SE3::Log(inv_mst * inv_pose0 * pose1);


        if(isnan(v_error(0))){
//            getchar();
//            std::cout << "\n vertex0 = \n " <<m_p_vertex0->r_v_State().transpose() << std::endl;
//            getchar();
//            std::cout << "\n vertex1 = \n " <<m_p_vertex1->r_v_State().transpose() << std::endl;
//            getchar();
            std::cout << "\n v_error = \n " << v_error.transpose() << std::endl;
            getchar();
//            std::cout << "inv_pose0 = \n" << inv_pose0 << std::endl;
//            getchar();
//            std::cout << "pose1 = \n" << pose1 << std::endl;
//            getchar();
//            std::cout << "inv_mst = \n" << inv_mst << std::endl;
//            getchar();
            std::cout << "Log(T): T = \n" << inv_mst * inv_pose0 * pose1 << std::endl;
            getchar();
        }


        r_t_jacobian0 = SE3::InvJl(v_error) * SE3::Ad(inv_mst);
        r_t_jacobian1 = -SE3::InvJr(v_error);
        r_v_error = v_error;


     //   std::cout << "\n r_t_jacobian1 = \n " <<r_t_jacobian1 << std::endl;


	}

	/**
	 *	@brief calculates \f$\chi^2\f$ error
	 *	@return Returns (unweighted) \f$\chi^2\f$ error for this edge.
	 */
	inline double f_Chi_Squared_Error() const
	{
        // get matrix form of measurements and poses
        Eigen::Matrix4d inv_mst = SE3::Inv(SE3::V2M(m_v_measurement));  /* inv(measurement) */
        Eigen::Matrix4d inv_pose0 = SE3::Inv(SE3::V2M(m_p_vertex0->r_v_State())); /* inv(pose0) */
        Eigen::Matrix4d pose1 = SE3::V2M(m_p_vertex1->r_v_State()); /* pose1 */
        // error is defined by SE2 logarithm mapping
        Eigen::Matrix<double, 6, 1> v_error = SE3::Log(inv_mst * inv_pose0 * pose1);
        //return (v_error.transpose() * m_t_sigma_inv).dot(v_error); // ||z_i - h_i(O_i)||^2 lambda_i
        // return v_error.dot(m_t_sigma_inv * v_error);

        return v_error.transpose() * m_t_sigma_inv * v_error;
	}
};


/** @} */ // end of group

/** \addtogroup parser
 *	@{
 */

/**
 *	@brief edge traits for SE(3) solver
 */
    template <class CParsedStructure>
    class CSE3OnlyPoseEdgeTraits {
    public:
        typedef CFailOnEdgeType _TyEdge; /**< @brief it should fail on unknown edge types */

        /**
         *	@brief gets reason for error
         *	@return Returns const null-terminated string, containing
         *		description of the error (human readable).
         */
        static const char *p_s_Reason()
        {
            return "unknown edge type occurred";
        }
    };

/**
 *	@brief edge traits for SE(3) solver (specialized for CParser::TEdge3D)
 */
    template <>
    class CSE3OnlyPoseEdgeTraits<CParserBase::TEdge3D> {
    public:
        typedef CEdgePose3D _TyEdge; /**< @brief the edge type to construct from the parsed type */
    };

#if 0
    /**
 *	@brief edge traits for SE(3) solver (specialized for CParser::TEdge3D)
 */
template <>
class CSE3OnlyPoseEdgeTraits<CParserBase::TVertex3D> { // nonsense, CParserBase::TVertex3D is not an edge, this does not belong here
public:
	typedef CIgnoreEdgeType _TyEdge; /**< @brief the edge type to construct from the parsed type */
};
#endif // 0

/**
 *	@brief edge traits for SE(3) solver
 */
    template <class CParsedStructure>
    class CSE3LandmarkPoseEdgeTraits {
    public:
        typedef CFailOnEdgeType _TyEdge; /**< @brief it should fail on unknown edge types */

        /**
         *	@brief gets reason for error
         *	@return Returns const null-terminated string, containing
         *		description of the error (human readable).
         */
        static const char *p_s_Reason()
        {
            return "unknown edge type occurred";
        }
    };

/**
 *	@brief edge traits for SE(3) solver (specialized for CParser::TEdge3D)
 */
    template <>
    class CSE3LandmarkPoseEdgeTraits<CParserBase::TEdge3D> {
    public:
        typedef CEdgePose3D _TyEdge; /**< @brief the edge type to construct from the parsed type */
    };


/** @} */ // end of group

#endif // !__SE3_TYPES_INCLUDED
