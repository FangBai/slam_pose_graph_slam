/*
								+-----------------------------------+
								|                                   |
								|       ***  SE(2) types  ***       |
								|                                   |
								|  Copyright  (c) -tHE SWINe- 2012  |
								|                                   |
								|            SE2_Types.h            |
								|                                   |
								+-----------------------------------+
*/

#pragma once
#ifndef __SE2_PRIMITIVE_TYPES_INCLUDED
#define __SE2_PRIMITIVE_TYPES_INCLUDED

/**
 *	@file include/slam/SE2_Types.h
 *	@brief SE(2) primitive types
 *	@author -tHE SWINe-
 *	@date 2012-09-03
 *
 *	@date 2012-09-12
 *
 *	Fixed linux build issues (vertex constructors took reference to Eigen::Vector, instead
 *	of const reference, g++ was unable to pass reference to a temporary object instance
 *	created on stack).
 *
 *	Added support for aligned edge and vertex types for the use of SSE2.
 *
 *	t_odo Write base edge template, that would inherit from CBaseEdge, and would implement
 *		all the functionality, required by the linear solvers. The edge itself should only
 *		contain a single function which calculates jacobians and expectation.
 *	t_odo Do the same for CBaseVertex.
 *
 */


#include "slam/BaseTypes.h"

#include "slam/Parser.h" // parsed types passed to constructors

#include "LieGroup.h" /* include Lie Group implementation */



/** \addtogroup se2
 *	@{
 */

/**
 *	@brief SE(2) pose vertex type
 */
class CVertexPose2D : public CBaseVertexImpl<CVertexPose2D, 3> {


public:
	__GRAPH_TYPES_ALIGN_OPERATOR_NEW

	/**
	 *	@brief default constructor; has no effect
	 */
	inline CVertexPose2D()
	{}

	/**
	 *	@brief constructor; initializes state vector
	 *	@param[in] r_v_state is state vector initializer
	 */
	inline CVertexPose2D(const Eigen::Vector3d &r_v_state)
		:CBaseVertexImpl<CVertexPose2D, 3>(r_v_state)
	{}

	/**
	 *	@copydoc base_iface::CVertexFacade::Operator_Plus()
	 */
	inline void Operator_Plus(const Eigen::VectorXd &r_v_delta) // "smart" plus
	{
        Eigen::Vector3d cv_delta = r_v_delta.segment<3>(m_n_order);
		Eigen::Matrix3d Tmatr = SE2::V2M(m_v_state); // transform vector storage to matrix storage
		Tmatr = Tmatr * SE2::Exp(cv_delta); // update with exponential mapping
		m_v_state = SE2::M2V(Tmatr);  // transform matrix storage to vector storage
	}

	/**
	 *	@copydoc base_iface::CVertexFacade::Operator_Minus()
	 */
	inline void Operator_Minus(const Eigen::VectorXd &r_v_delta) // "smart" minus
	{
        Eigen::Vector3d cv_delta = r_v_delta.segment<3>(m_n_order);
        Eigen::Matrix3d Tmatr = SE2::V2M(m_v_state); // transform vector storage to matrix storage
        Tmatr = Tmatr * SE2::Exp(-cv_delta); // update with exponential mapping
        m_v_state = SE2::M2V(Tmatr);  // transform matrix storage to vector storage
	}
};


#if 0 // vertex traits unused now

/**
 *	@brief token for 2D pose vertex
 */
class _vertex_token_pose2d {} vertex_token_pose2d; /**< token for 2D pose vertex */


/**
 *	@brief token for 3D pose vertex
 */
class _vertex_token_pose3d {} vertex_token_pose3d; /**< token for 3D pose vertex */



/**
 *	@brief vertex type traits
 */
template <class CTypeToken>
class CVertexTypeTraits {};

/**
 *	@brief vertex type traits (specialization for _vertex_token_pose2d)
 */
template <>
class CVertexTypeTraits<_vertex_token_pose2d> {
public:
	typedef CVertexPose2D _TyVertex; /**< @brief vertex type associated with the token */
};


#endif // 0

/**
 *	@brief SE(2) pose-pose edge
 */
class CEdgePose2D : public CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D), 3> {
public:
	/**
	 *	@brief vertex initialization functor
	 *	Calculates vertex position from the first vertex and an XYT edge.
	 */
	class CRelative_to_Absolute_XYT_Initializer {
	protected:
		const Eigen::Vector3d &m_r_v_pose1; /**< @brief the first vertex */
		const Eigen::Vector3d &m_r_v_edge; /**< @brief the edge, shared by r_v_vertex1 and the vertex being initialized */

	public:
		/**
		 *	@brief default constructor
		 *
		 *	@param[in] r_v_vertex1 is the first vertex
		 *	@param[in] r_v_edge is the edge, shared by r_v_vertex1 and the vertex being initialized
		 */
		inline CRelative_to_Absolute_XYT_Initializer(const Eigen::Vector3d &r_v_vertex1,
			const Eigen::Vector3d &r_v_edge)
			:m_r_v_pose1(r_v_vertex1), m_r_v_edge(r_v_edge)
		{}

		/**
		 *	@brief function operator
		 *	@return Returns the value of the vertex being initialized.
		 */
		inline operator CVertexPose2D() const
		{
			Eigen::Matrix3d Pose2 = SE2::V2M(m_r_v_pose1) * SE2::V2M(m_r_v_edge);
			// initialized by compounding first pose and the edge
			return CVertexPose2D(SE2::M2V(Pose2));
		}
	};

public:
	__GRAPH_TYPES_ALIGN_OPERATOR_NEW

	/**
	 *	@brief default constructor; has no effect
	 */
	inline CEdgePose2D()
	{}

	/**
	 *	@brief constructor; converts parsed edge to edge representation
	 *
	 *	@tparam CSystem is type of system where this edge is being stored
	 *
	 *	@param[in] r_t_edge is parsed edge
	 *	@param[in,out] r_system is reference to system (used to query edge vertices)
	 */
	template <class CSystem>
	CEdgePose2D(const CParserBase::TEdge2D &r_t_edge, CSystem &r_system)
		:CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D), 3>(r_t_edge.m_n_node_0,
		r_t_edge.m_n_node_1, r_t_edge.m_v_delta, r_t_edge.m_t_inv_sigma)
	{
		// r_Get_Vertex: return existing vertex, or create a new vertex if it does not exist
		m_p_vertex0 = &r_system.template r_Get_Vertex<CVertexPose2D>(r_t_edge.m_n_node_0,
			CInitializeNullVertex<>());  // initialize to zero if first pose does not exist
		m_p_vertex1 = &r_system.template r_Get_Vertex<CVertexPose2D>(r_t_edge.m_n_node_1,
			CRelative_to_Absolute_XYT_Initializer(m_p_vertex0->r_v_State(), r_t_edge.m_v_delta));
		// initialize second pose by compounding first pose and the measurement edge
		// get vertices (initialize if required)
		// "template" is required by g++, otherwise gives "expected primary-expression before '>' token"

		//_ASSERTE(r_system.r_Vertex_Pool()[r_t_edge.m_n_node_0].n_Dimension() == 3); // get the vertices from the vertex pool to ensure a correct type is used, do not use m_p_vertex0 / m_p_vertex1 for this
		//_ASSERTE(r_system.r_Vertex_Pool()[r_t_edge.m_n_node_1].n_Dimension() == 3);
		// make sure the dimensionality is correct (might not be)
		// this fails with const vertices, for obvious reasons. with the thunk tables this can be safely removed.
	}

	/**
	 *	@brief constructor; initializes edge with data
	 *
	 *	@tparam CSystem is type of system where this edge is being stored
	 *
	 *	@param[in] n_node0 is (zero-based) index of the first (origin) node
	 *	@param[in] n_node1 is (zero-based) index of the second (endpoint) node
	 *	@param[in] v_delta is vector of delta x position, delta y-position and delta-theta
	 *	@param[in] r_t_inv_sigma is the information matrix
	 *	@param[in,out] r_system is reference to system (used to query edge vertices)
	 */
	template <class CSystem>
	CEdgePose2D(size_t n_node0, size_t n_node1, const Eigen::Vector3d &v_delta,
		const Eigen::Matrix3d &r_t_inv_sigma, CSystem &r_system)
		:CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D), 3>(n_node0,
		n_node1, v_delta, r_t_inv_sigma)
	{
		// r_Get_Vertex: return existing vertex, or create a new vertex if it does not exist
		m_p_vertex0 = &r_system.template r_Get_Vertex<CVertexPose2D>(n_node0,
			CInitializeNullVertex<>());  // initialize to zero if first pose does not exist
		m_p_vertex1 = &r_system.template r_Get_Vertex<CVertexPose2D>(n_node1,
			CRelative_to_Absolute_XYT_Initializer(m_p_vertex0->r_v_State(), v_delta));
		// initialize second pose by compounding first pose and the measurement edge
		// get vertices (initialize if required)
		// "template" is required by g++, otherwise gives "expected primary-expression before '>' token"

		//_ASSERTE(r_system.r_Vertex_Pool()[n_node0].n_Dimension() == 3); // get the vertices from the vertex pool to ensure a correct type is used, do not use m_p_vertex0 / m_p_vertex1 for this
		//_ASSERTE(r_system.r_Vertex_Pool()[n_node1].n_Dimension() == 3);
		// make sure the dimensionality is correct (might not be)
		// this fails with const vertices, for obvious reasons. with the thunk tables this can be safely removed.
	}

	/**
	 *	@brief updates the edge with a new measurement
	 *	@param[in] r_t_edge is parsed edge
	 */
	inline void Update(const CParserBase::TEdge2D &r_t_edge)
	{
		CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D),
			3>::Update(r_t_edge.m_v_delta, r_t_edge.m_t_inv_sigma);
	}

	/**
	 *	@brief updates the edge with new measurement
	 *
	 *	@param[in] r_v_measurement is the measurement vector
	 *	@param[in] r_t_sigma_inv is inverse sigma matrix
	 */
	inline void Update(const _TyStorageVector &r_v_measurement, const _TyMatrix &r_t_sigma_inv) // note that this is only required by MSVC which somehow does not see CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D), 3>::Update()
	{
		CBaseEdgeImpl<CEdgePose2D, MakeTypelist(CVertexPose2D, CVertexPose2D),
			3>::Update(r_v_measurement, r_t_sigma_inv);
	}

	/**
	 *	@brief calculates jacobians, expectation and error
	 *
	 *	@param[out] r_t_jacobian0 is jacobian, associated with the first vertex
	 *	@param[out] r_t_jacobian1 is jacobian, associated with the second vertex
	 *	@param[out] r_v_expectation is expecation vector
	 *	@param[out] r_v_error is error vector
	 */
	inline void Calculate_Jacobians_Expectation_Error(Eigen::Matrix3d &r_t_jacobian0,
		Eigen::Matrix3d &r_t_jacobian1, Eigen::Vector3d &r_v_expectation,
		Eigen::Vector3d &r_v_error) const
	{
		// get matrix form of measurements and poses
		Eigen::Matrix3d inv_mst = SE2::Inv(SE2::V2M(m_v_measurement));  /* inv(measurement) */
		Eigen::Matrix3d inv_pose0 = SE2::Inv(SE2::V2M(m_p_vertex0->r_v_State())); /* inv(pose0) */
		Eigen::Matrix3d pose1 = SE2::V2M(m_p_vertex1->r_v_State()); /* pose1 */
		// compute r_v_expectation
		r_v_expectation = SE2::M2V(inv_pose0 * pose1); // why need r_v_expectation, for what?
		// compute linearization Jacobians and error
		// Linear System || Jacobian * x - error ||
		Eigen::Vector3d v_error = SE2::Log(inv_mst * inv_pose0 * pose1);
		r_t_jacobian0 = SE2::InvJl(v_error) * SE2::Ad(inv_mst);
		r_t_jacobian1 = -SE2::InvJr(v_error);
        r_v_error = v_error;
	}

	/**
	 *	@brief calculates \f$\chi^2\f$ error
	 *	@return Returns (unweighted) \f$\chi^2\f$ error for this edge.
	 */
	inline double f_Chi_Squared_Error() const
	{
		// get matrix form of measurements and poses
		Eigen::Matrix3d inv_mst = SE2::Inv(SE2::V2M(m_v_measurement));  /* inv(measurement) */
		Eigen::Matrix3d inv_pose0 = SE2::Inv(SE2::V2M(m_p_vertex0->r_v_State())); /* inv(pose0) */
		Eigen::Matrix3d pose1 = SE2::V2M(m_p_vertex1->r_v_State()); /* pose1 */
		// error is defined by SE2 logarithm mapping
		Eigen::Vector3d v_error = SE2::Log(inv_mst * inv_pose0 * pose1);
		//return (v_error.transpose() * m_t_sigma_inv).dot(v_error); // ||z_i - h_i(O_i)||^2 lambda_i
		// return v_error.dot(m_t_sigma_inv * v_error);
        return v_error.transpose() * m_t_sigma_inv * v_error;
	}
};


/** @} */ // end of group


/** \addtogroup parser
 *	@{
 */

/**
 *	@brief edge traits for SE(2) solver
 */
template <class CParsedStructure>
class CSE2EdgeTraits {
public:
	typedef CFailOnEdgeType _TyEdge; /**< @brief it should fail on unknown edge types */

	/**
	 *	@brief gets reason for error
	 *	@return Returns const null-terminated string, containing
	 *		description of the error (human readable).
	 */
	static const char *p_s_Reason()
	{
		return "unknown edge type occurred";
	}
};

/**
 *	@brief edge traits for SE(2) solver (specialized for CParser::TEdge2D)
 */
template <>
class CSE2EdgeTraits<CParserBase::TEdge2D> {
public:
	typedef CEdgePose2D _TyEdge; /**< @brief the edge type to construct from the parsed type */
};


/**
 *	@brief edge traits for SE(2) pose-only solver (specialized for CParser::TVertex2D)
 */
template <>
class CSE2EdgeTraits<CParserBase::TVertex2D> {
public:
	typedef CIgnoreEdgeType _TyEdge; /**< @brief the edge type to construct from the parsed type */
};

/**
 *	@brief edge traits for SE(2) pose-only solver
 */
template <class CParsedStructure>
class CSE2OnlyPoseEdgeTraits {
public:
	typedef CFailOnEdgeType _TyEdge; /**< @brief it should fail on unknown edge types */

	/**
	 *	@brief gets reason for error
	 *	@return Returns const null-terminated string, containing
	 *		description of the error (human readable).
	 */
	static const char *p_s_Reason()
	{
		return "unknown edge type occurred";
	}
};

/**
 *	@brief edge traits for SE(2) pose-only solver (specialized for CParser::TEdge2D)
 */
template <>
class CSE2OnlyPoseEdgeTraits<CParserBase::TEdge2D> {
public:
	typedef CEdgePose2D _TyEdge; /**< @brief the edge type to construct from the parsed type */
};

/**
 *	@brief edge traits for SE(2) pose-only solver (specialized for CParser::TVertex2D)
 */
template <>
class CSE2OnlyPoseEdgeTraits<CParserBase::TVertex2D> {
public:
	typedef CIgnoreEdgeType _TyEdge; /**< @brief the edge type to construct from the parsed type */
};

/**
 *	@brief edge traits for SE(2) pose-only solver (specialized for CParser::TLandmark2D)
 */
template <>
class CSE2OnlyPoseEdgeTraits<CParserBase::TLandmark2D_XY> {
public:
	typedef CFailOnEdgeType _TyEdge; /**< @brief the edge type to construct from the parsed type */

	/**
	 *	@brief gets reason for error
	 *	@return Returns const null-terminated string, containing
	 *		description of the error (human readable).
	 */
	static const char *p_s_Reason()
	{
		return "landmark edges not permitted in pose-only solver";
	}
};

/**
 *	@brief edge traits for SE(2) pose-only solver (specialized for CParser::TLandmark2D)
 */
template <>
class CSE2OnlyPoseEdgeTraits<CParserBase::TLandmark2D_RB> {
public:
	typedef CFailOnEdgeType _TyEdge; /**< @brief the edge type to construct from the parsed type */

	/**
	 *	@brief gets reason for error
	 *	@return Returns const null-terminated string, containing
	 *		description of the error (human readable).
	 */
	static const char *p_s_Reason()
	{
		return "landmark edges not permitted in pose-only solver";
	}
};

/** @} */ // end of group

#endif // !__SE2_PRIMITIVE_TYPES_INCLUDED
