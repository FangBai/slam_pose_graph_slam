//
// Created by fang on 20/01/18.
//

#ifndef SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H
#define SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H



#include <stdio.h> // printf
#include <algorithm>
#include <numeric>
#include <queue>
#ifdef _OPENMP
#include <omp.h>
#endif // _OPENMP
#include <vector>
// #include <list>

#include "PoseGraphOptimizer.h"



template <const bool b_2D_SLAM = true, const bool b_Incremental_Solver = false>
class CPoseGraphOptimizerOnline : public CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver> {

public:

    typedef typename CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::_TyMatrix  _TyMatrix; /**< @copydoc _TyEdgeData::_TyMatrix */
    typedef typename CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::_TyVector _TyVector; /**< @copydoc _TyEdgeData::_TyVector */

    typedef Eigen::Matrix<double, (b_2D_SLAM)? 3 : 4, (b_2D_SLAM)? 3 : 4> _TyPoseMatrix;

    typedef typename LieGroup<b_2D_SLAM>::_TySE SE;  // Lie Group SE Operations


    /**
     *	@brief decision type of edges stored as enum
     */
    enum EdgeStatus{
        status_accepted = 0, /**< @brief accepted as an inlier */
        status_pending = 1, /**< @brief cannot decide yet, decision pending */
        status_rejected = 2 /**< @brief rejected as an outlier */
    };


    struct TPendingEdgeInfo{
        EdgeStatus e_edge_status;
        size_t vertex[2];
        _TyVector v_measurement;
        _TyMatrix t_Information;
        _TyMatrix t_Sigma11;
        _TyMatrix t_Sigma12;
        _TyMatrix t_Sigma22;
    };


    struct Chi2TestDOF3{
        static constexpr double chi2_test_90 = 6.251;  //0.90
        static constexpr double chi2_test_95 = 7.815;  // 0.95
        static constexpr double chi2_test_975 = 9.348;  // 0.975
        static constexpr double chi2_test_99 = 11.345;  // 0.99
        static constexpr double chi2_test_999 = 16.266; // 0.999
    };

    
    struct TEdgeInfo; // forward declaration for edge information type


    CPoseGraphOptimizerOnline(bool b_linearize_measurement, size_t n_max_iteration_num = 5, double f_error_threshold = .001)
            :CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>(n_max_iteration_num, f_error_threshold),
             m_b_linearize_measurement(b_linearize_measurement)
    {
        m_EdgeInfoRecord = std::vector<TEdgeInfo>();
        m_EdgeInfoRecord.reserve(30000);
    //    m_EdgeInfoRecord.clear();
    }

    virtual ~CPoseGraphOptimizerOnline() {}


    int FeedDataOnline(size_t n_vertex_id1, size_t n_vertex_id2, const _TyVector &v_measurement, const _TyMatrix &t_information)
    {
        bool isOdometry = (n_vertex_id2 - n_vertex_id1 == 1 || n_vertex_id1 - n_vertex_id2 == 1);
        double used_time = .0;
        this->tic.Accum_DiffSample(used_time);
      //  printf("Process edge <%d, %d> \n", (int) n_vertex_id1, (int) n_vertex_id2);
        double delta_objFunc_predicted = .0; //predicted objFunc change
        EdgeStatus e_edge_status = status_accepted;
        if( !isOdometry ){ /* loop-closure edges */
            delta_objFunc_predicted = this->PredictObjFuncChange(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
            e_edge_status = EdgeDecision(delta_objFunc_predicted); // Edge decision according to objFunc change
        }
//        double pre_objFunc = this->Get_ObjFunc(); // previous objFunc before optimization
        // different operations according to EdgeStatus
        if (e_edge_status == status_accepted) {
            if(b_Incremental_Solver){
                // using incremental solver
                this->Incremental_Step(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
                this->Optimize(1, .001); // allow one iteration to increase accuracy
                // observations: zero iteration is not accurate because the solver does not update
                // only one iteration is pretty good for online solver, usually converge well
            }else{
                // using batch solver
                this->Add_Edge(n_vertex_id1, n_vertex_id2, v_measurement, t_information);
                this->Optimize(10, .00001); // batch optimize until convergence: error threshold: 1e-6
            }
        } else if (e_edge_status == status_rejected) {  // Definitely Wrong.
            // Definitely wrong, set rejected and never consider again
            // Do not maintain a list for this kind of measurement: won't consider again
            // Ridiculously larger than a threshold
            // Marked as rejected, in the edge_info flag. do not optimize
        } else if (e_edge_status == status_pending) {  // Ambiguous measurement
            // This is reserved for the "ambiguous measurement", cannot make a decision now
            // Decision pending until more information is acquired
            // Marked as pending, do not optimize
            // Add to the pending list, check this edge latter?
            TPendingEdgeInfo pendingEdge;
            pendingEdge.e_edge_status = status_pending;
            pendingEdge.vertex[0] = n_vertex_id1;
            pendingEdge.vertex[1] = n_vertex_id2;
            pendingEdge.v_measurement = v_measurement;
            pendingEdge.t_Information = t_information;
            pendingEdge.t_Sigma11 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id1);
            pendingEdge.t_Sigma12 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id2);
            pendingEdge.t_Sigma22 = this->Get_CovarianceBlock(n_vertex_id2, n_vertex_id2);
            m_PendingEdgeInfoCache.push_back(pendingEdge);
        }
//        double cur_objFunc = this->Get_ObjFunc(); // current objFunc after optimization
//        double delta_objFunc_real = cur_objFunc - pre_objFunc; // real objFunc change
        this->tic.Accum_DiffSample(used_time);
        if( !isOdometry && e_edge_status != status_pending )
        {
            TEdgeInfo edge_info;
            edge_info.vertex[0] = n_vertex_id1;
            edge_info.vertex[1] = n_vertex_id2;
            edge_info.status = e_edge_status;
            edge_info.objFuncChange_predicted = delta_objFunc_predicted;
            edge_info.timing = used_time;
           //
            edge_info.usedIters = this->Get_Iters();
            //
            m_EdgeInfoRecord.push_back(edge_info);
        }
        return e_edge_status;
    }


    int CheckPendingEdge ()
    {
        int n_count = 0;
        for (size_t i = 0, n = m_PendingEdgeInfoCache.size(); i < n; ++i)
        {
            double used_time = .0;
            this->tic.Accum_DiffSample(used_time);
            //
            TPendingEdgeInfo & e = m_PendingEdgeInfoCache[i];
            double objFuncChange = EvaluatePendingEdge(i);
            e.e_edge_status = EdgeDecisionLoose(objFuncChange); // loose edge decision, accept as much as possible
            if(e.e_edge_status == status_accepted)
            {
                ++ n_count;
                this->Add_Edge(e.vertex[0], e.vertex[1], e.v_measurement, e.t_Information);
                this->Optimize(10, .00001); // batch optimize until convergence: error threshold: 1e-6
            }
            this->tic.Accum_DiffSample(used_time);
            //
            TEdgeInfo edge_info;
            edge_info.vertex[0] = e.vertex[0];
            edge_info.vertex[1] = e.vertex[1];
            edge_info.status = e.e_edge_status;
            edge_info.objFuncChange_predicted = objFuncChange;
            edge_info.timing = used_time;
            //
            edge_info.usedIters = this->Get_Iters();
            //
            m_EdgeInfoRecord.push_back(edge_info);
        }
        return n_count;
    }


    double PredictObjFuncChange(size_t n_vertex_id1, size_t n_vertex_id2, const _TyVector &v_measurement, const _TyMatrix &t_information)
    {
        // get pose estimates
        _TyVector v_pose1 = this->Get_Vertex(n_vertex_id1);
        _TyVector v_pose2 = this->Get_Vertex(n_vertex_id2);
        // get covariance of the estimates
        _TyMatrix Sigma11 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id1);
        _TyMatrix Sigma12 = this->Get_CovarianceBlock(n_vertex_id1, n_vertex_id2);
        _TyMatrix Sigma22 = this->Get_CovarianceBlock(n_vertex_id2, n_vertex_id2);
        // compute predicted objFunc change
        double obj_func_change = .0;
        //
        // matrix form of poses 3by3 or 4by4
        _TyPoseMatrix inv_mst = SE::Inv(SE::V2M(v_measurement));
        _TyPoseMatrix inv_pose1 = SE::Inv(SE::V2M(v_pose1));
        _TyPoseMatrix pose2 = SE::V2M(v_pose2);
        // intermediate vectors and matrices
        _TyVector mst_error;
        _TyMatrix J1, J2, J3, mst_error_cov;
        // measurement error
        mst_error = SE::Log(inv_mst * inv_pose1 * pose2);
        // J = [ J1, J2, J3]
        J1 = SE::InvJl(mst_error) * SE::Ad(inv_mst);
        J2 = -SE::InvJr(mst_error);
        if(m_b_linearize_measurement){
            J3 = SE::InvJl(mst_error);
        }else{
            J3 = _TyMatrix::Identity();
        }
        // measurement error covariance expansion
        mst_error_cov = J1 * Sigma11 * J1.transpose()
                        + J1 * Sigma12 * J2.transpose()
                        + J2 * Sigma12.transpose() * J1.transpose()
                        + J2 * Sigma22 * J2.transpose()
                        + J3 * t_information.inverse() * J3.transpose();
        // predicted objective function change
        obj_func_change = mst_error.dot (mst_error_cov.inverse() * mst_error);
        return obj_func_change;
    }

    double EvaluatePendingEdge(size_t item_id)
    {
        TPendingEdgeInfo & pendingEdge = m_PendingEdgeInfoCache[item_id];
        // get current pose estimates
        _TyVector v_pose1 = this->Get_Vertex(pendingEdge.vertex[0]);
        _TyVector v_pose2 = this->Get_Vertex(pendingEdge.vertex[1]);
        // compute predicted objFunc change
        double obj_func_change = .0;
        //
        // matrix form of poses 3by3 or 4by4
        _TyPoseMatrix inv_mst = SE::Inv(SE::V2M(pendingEdge.v_measurement));
        _TyPoseMatrix inv_pose1 = SE::Inv(SE::V2M(v_pose1));
        _TyPoseMatrix pose2 = SE::V2M(v_pose2);
        // intermediate vectors and matrices
        _TyVector mst_error;
        _TyMatrix J1, J2, J3, mst_error_cov;
        // measurement error
        mst_error = SE::Log(inv_mst * inv_pose1 * pose2);
        // J = [ J1, J2, J3]
        J1 = SE::InvJl(mst_error) * SE::Ad(inv_mst);
        J2 = -SE::InvJr(mst_error);
        if(m_b_linearize_measurement){
            J3 = SE::InvJl(mst_error);
        }else{
            J3 = _TyMatrix::Identity();
        }
        // measurement error covariance expansion
        mst_error_cov = J1 * pendingEdge.t_Sigma11 * J1.transpose()
                        + J1 * pendingEdge.t_Sigma12 * J2.transpose()
                        + J2 * pendingEdge.t_Sigma12.transpose() * J1.transpose()
                        + J2 * pendingEdge.t_Sigma22 * J2.transpose()
                        + J3 * pendingEdge.t_Information.inverse() * J3.transpose();
        // predicted objective function change
        obj_func_change = mst_error.dot (mst_error_cov.inverse() * mst_error);
        return obj_func_change;
    }

    EdgeStatus EdgeDecision(double delta_obj_func) {
        // outlier free cases
        if(delta_obj_func > Chi2TestDOF3::chi2_test_999) // chi3(0.999)
            return status_rejected;
        else if(delta_obj_func < Chi2TestDOF3::chi2_test_95) //chi3(0.95)
            return status_accepted;
        else
            return status_pending;
    }


    EdgeStatus EdgeDecisionLoose(double delta_obj_func) {
        // outlier free cases
        if(delta_obj_func > Chi2TestDOF3::chi2_test_999) // chi3(0.999)
            return status_rejected;
        else if(delta_obj_func < Chi2TestDOF3::chi2_test_99) //chi3(0.99)
            return status_accepted;
        else
            return status_pending;
    }


    void SaveEdgeInfoRecord(const char *p_s_filename)
    {
        std::FILE * record_file;
        record_file = std::fopen(p_s_filename, "w");
        for (size_t i = 0, n = m_EdgeInfoRecord.size(); i < n; ++ i)
        {
            TEdgeInfo & edge_info = m_EdgeInfoRecord[i];
            std::fprintf(record_file, "%zu %zu %zu %d"
                                 " %.8f %.8f"
                                 " %zu \n",
                         i+1, edge_info.vertex[0], edge_info.vertex[1], edge_info.status,
                         edge_info.objFuncChange_predicted, edge_info.timing,
                         edge_info.usedIters
            );
        }
        std::fclose(record_file);
        printf("Edge information has been saved into the file: \"%s\" \n", p_s_filename);
        printf("\t Format:\n\t [index  vertex0  vertex1  edge_status(0=accepted 1=pending 2=rejected)  predictedObjectiveFunctionChange  timing  usedIterations] \n");
    }


private:

    bool m_b_linearize_measurement;  // Either is good!!!

    std::vector<CPoseGraphOptimizerOnline::TEdgeInfo> m_EdgeInfoRecord;

    std::vector<CPoseGraphOptimizerOnline::TPendingEdgeInfo> m_PendingEdgeInfoCache;

};


// structure to preserve run time information regarding objFuncChanges
template <const bool b_2D_SLAM, const bool b_Incremental_Solver>
struct CPoseGraphOptimizerOnline<b_2D_SLAM, b_Incremental_Solver>::TEdgeInfo
{
    size_t vertex[2];
    CPoseGraphOptimizerOnline::EdgeStatus status;
    double objFuncChange_predicted;
    double timing;
    size_t usedIters;
};



#endif //SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZERONLINE_H
