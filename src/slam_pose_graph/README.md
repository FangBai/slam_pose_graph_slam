## Executables 

After compilation with master/IROS2018 branch, you will find the following two exectuables in `slam_plus_plus_root_dir/bin`:

* **slam_pose_graph**, which implements the change of optimal values used for IROS 2018
* **slam_robust_pgo**, which implements various M-estimator options

<br>

To use the approach in IROS 2018, it suffices to include the head file
[`src/slam_pose_graph/PoseGraphOptimizerOnline.h`](PoseGraphOptimizerOnline.h)

<br>

## Optimizer 
In SLAM++, there exists two types of matrix factorization methods
* standard Choleksy, called **Lambda Solver**
* incremental Cholesky, called **FastL Solver** 
The incremental Choleksy solvers run faster by sacrificing some accruacy. You can enable this option by passing `-is` in the command line argment. For the best accuracy, we use the standard Cholesky in the paper.

## Linearize measurement
We also tried the case of linearizing the measurement fucntion, when trying to come up the closed-form equation. **This is not required finally**.

However, if one is interested in experimenting different linearization strategies, it is possible to enable this feature by passing `-lm` in the command line argument. In essence, this adapts the measurement covariance with Jaocbian $J_3$.
```
    mst_error = SE::Log(inv_mst * inv_pose1 * pose2);
    // J = [ J1, J2, J3]
    J1 = SE::InvJl(mst_error) * SE::Ad(inv_mst);
    J2 = -SE::InvJr(mst_error);
    if(m_b_linearize_measurement){
        J3 = SE::InvJl(mst_error);
    }else{
        J3 = _TyMatrix::Identity();
    }
    // measurement error covariance expansion
    mst_error_cov = J1 * Sigma11 * J1.transpose()
                    + J1 * Sigma12 * J2.transpose()
                    + J2 * Sigma12.transpose() * J1.transpose()
                    + J2 * Sigma22 * J2.transpose()
                    + J3 * t_information.inverse() * J3.transpose();
    // predicted objective function change
    obj_func_change = mst_error.dot (mst_error_cov.inverse() * mst_error);
```



<br>

## After predicting the objective function change, we made 3 types of decisions for each edge from chi-square difference tests:
```
    enum EdgeStatus{
        status_accepted = 0, /**< @brief accepted as an inlier */
        status_pending = 1, /**< @brief cannot decide yet, decision pending */
        status_rejected = 2 /**< @brief rejected as an outlier */
    };
```
These decisions and the corresponding **predicted objective function** can be exported to a file, by passing a file name to the `-of` argument, as `-of obj_file_name.txt`.


<br>

Inside the code, the print function is implemented as follows:
```
void SaveEdgeInfoRecord(const char *p_s_filename)
{
    std::FILE * record_file;
    record_file = std::fopen(p_s_filename, "w");
    for (size_t i = 0, n = m_EdgeInfoRecord.size(); i < n; ++ i)
    {
        TEdgeInfo & edge_info = m_EdgeInfoRecord[i];
        std::fprintf(record_file, "%zu %zu %zu %d"
                                " %.8f %.8f"
                                " %zu \n",
                        i+1, edge_info.vertex[0], edge_info.vertex[1], edge_info.status,
                        edge_info.objFuncChange_predicted, edge_info.timing,
                        edge_info.usedIters
        );
    }
    std::fclose(record_file);
    printf("Edge information has been saved into the file: \"%s\" \n", p_s_filename);
    printf("\t Format:\n\t [index  vertex0  vertex1  edge_status(0=accepted 1=pending 2=rejected)  predictedObjectiveFunctionChange  timing  usedIterations] \n");
}
```

The output format per line in the txt file is as follows:
```
i+1, edge_info.vertex[0], edge_info.vertex[1], edge_info.status(), edge_info.objFuncChange_predicted, edge_info.timing, edge_info.usedIters
```

<br>

## The command line arguments for `slam_pose_graph` for the input and output:
```
    void PrintHelp(){
        printf("use: slam_pose_graph <input-file> [options]\n\n"
               "where:\n"
               "--help | -h shows this help screen\n"
               "--incremental-solver | -is   initialize an incremental optimization core, default is a batch core \n"
               "--linearize-measurement | -lm   linearize measurements in formulas to predict objFunc change\n"
               "--objFunc-output-file | -of   output file of objective function changes and edge decision status\n"
               "--vertices-output-file | -vf   output file of states of vertices \n"
               "--summary-output-file | -sf   output file of summary information\n"
        );
    }
```

in particular, the relevant interface arguments and the corresponding print function
```
    if(params.p_s_vertices_file != NULL)
        optimizer.SaveSolution(params.p_s_vertices_file);
    if(params.p_s_objFunc_file !=NULL)
        optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
    if(params.p_s_summary_file !=NULL)
        optimizer.SaveSummary(params.p_s_summary_file);
```


<br>

## PGO solution is saved as a vertex file. The rotation component is saved using the axis-angle represenation.

```
    virtual void SaveSolution(const char *p_s_filename)
    {
        ...
            if(b_2D_SLAM){
                std::fprintf(output_file,
                             "VERTEX2 %zu %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2]
                );
            }else{
                std::fprintf(output_file,
                             "VERTEX3 %zu %.8f %.8f %.8f %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2], v[3], v[4], v[5]
                );
            }
        }
        std::fclose(output_file);
        printf("Estimates of vertices have already been saved into the file: \"%s\" \n", p_s_filename);
    }
```

