//
// Created by fang on 9/01/18.
//

#ifndef SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZER_H
#define SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZER_H

//#define POSE_GRAPH_SLAM_COMPUTE_OBJECTIVE_FUNCTION_FROM_LINEAR_SYSTEM

#include "slam/LinearSolver_UberBlock.h" // linear solver
#include "slam/ConfigSolvers.h" // nonlinear graph solvers
#include "slam/Marginals.h" // covariance calculation
#include "slam/Distances.h" // distance calculation
#include "slam_app/ParsePrimitives.h"
#include "slam/Parser.h"
#include "eigen/Eigen/SVD" // JacobiSVD
#include "slam/MemUsage.h"


#include "LieGroup.h" // Lie Group implementation
#include "LieGroupSE2_Types.h" // SE(2) types
#include "LieGroupSE3_Types.h" // SE(3) types




template <const bool b_2D_SLAM = true, const bool b_Incremental_Solver = false>
class CPoseGraphOptimizer{

public:

    template <const bool t_b_2D_SLAM, const bool t_b_Incremental_Solver>
    class CPoseGraphOptimizerCore; // forward declaration

    /**
     *	@brief parameters stored as enum
     */
    enum {
        n_pose_dimension = (b_2D_SLAM)? 3 : 6, /**< @brief vertex dimension */
    };

    typedef Eigen::Matrix<double, n_pose_dimension, n_pose_dimension> _TyMatrix; /**< @copydoc _TyEdgeData::_TyMatrix */
    typedef Eigen::Matrix<double, n_pose_dimension, 1> _TyVector; /**< @copydoc _TyEdgeData::_TyVector */

    typedef CPoseGraphOptimizerCore<b_2D_SLAM, b_Incremental_Solver> OptimizerType;
    typedef typename OptimizerType::CSystemType CSystemType;
    typedef typename OptimizerType::CNonlinearSolverType CNonlinearSolverType;

    typedef typename OptimizerType::_TyVertexPose _TyVertexPose;
    typedef typename OptimizerType::_TyEdgePose _TyEdgePose;
    typedef typename OptimizerType::CSystemType::_TyVertexMultiPool _TyVertexMultiPool;



protected:

    OptimizerType *m_p_optimizer;

    CTimer m_timer;

    CTimerSampler tic;

    double f_m_total_time;



//    CTimer timer; // a global timer object (don't want to construct many of those, want to share it)
//    double f_time_a = 0, f_time_b = 0; // time spent in different parts of the algorithm
//    double f_total_time; // total time (we want to represent it explicitly to avoid later mistakes)
//    CTimerSampler tic(timer); // timing starts here
//    AlgorithmPartA();
//    tic.Accum_DiffSample(f_time_a); // like a "toc" and a next "tic"
//    AlgorithmPartB();
//    tic.Accum_DiffSample(f_time_b); // like a "toc"
//    tic.Accum_CumTime_LastSample(f_total_time); // does not imply a sample (the last sample is used)



public:

    CPoseGraphOptimizer(size_t n_max_iteration_num = 5, double f_min_dx_norm = .001):
            tic(m_timer), f_m_total_time(0)
    {
        m_p_optimizer = new OptimizerType(n_max_iteration_num, f_min_dx_norm);
        printf("Pose Graph Optimizer Initialized! \n");
    }

    virtual ~CPoseGraphOptimizer()
    {
        delete m_p_optimizer;
    }

    inline size_t n_Vertex_Num()
    {
        CSystemType &system = m_p_optimizer->r_System();
        return system.n_Vertex_Num();
    }

    inline size_t n_Edge_Num()
    {
        CSystemType &system = m_p_optimizer->r_System();
        return system.n_Edge_Num();
    }

    inline void Optimize(size_t n_max_iteration_num = 5, double f_min_dx_norm = .001)
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        solver.Optimize(n_max_iteration_num, f_min_dx_norm);
    }

    inline _TyEdgePose& Add_Edge(size_t n_vertex_id1, size_t n_vertex_id2, _TyVector v_measurement, _TyMatrix t_information)
    {
        CSystemType &system = m_p_optimizer->r_System();
        return system.r_Add_Edge(_TyEdgePose(n_vertex_id1, n_vertex_id2, v_measurement, t_information, system));
    }

    inline _TyVector Get_Vertex(size_t id)
    {
        CSystemType &system = m_p_optimizer->r_System();
        _TyVertexMultiPool &vertex_pool = system.r_Vertex_Pool();
        _TyVertexPose & vertex = vertex_pool.template r_At<_TyVertexPose> (id);
        return vertex.v_State();
    }

    inline _TyMatrix Get_CovarianceBlock(size_t blk_row_id, size_t blk_col_id) // SigmaBlock(blkrow i, blkcol j)
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        const CUberBlockMatrix &r_marginals = solver.r_MarginalCovariance().r_SparseMatrix();
        _ASSERTE(r_marginals.n_BlockColumn_Num() == system.r_Vertex_Pool().n_Size());
        return r_marginals.t_GetBlock_Log(blk_row_id, blk_col_id);
    }

    // objective function of current nonlinear system
    inline double Get_ObjFunc()
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        return solver.f_Chi_Squared_Error_Denorm();
    }

    // objective function of current linear system
    inline double Get_ObjFunc_Linear()
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        return solver.f_Chi_Squared_Error_Linear();
    }

    // Iterations consumed
    inline double Get_Iters()
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        return solver.n_Iteration_Num();
    }

    // linearization Change of between two consecutive iterations
    inline double Get_LinearizationChange()
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        return solver.f_LineariationChange();
    }


    void Incremental_Step(size_t n_vertex_id1, size_t n_vertex_id2, _TyVector v_measurement, _TyMatrix t_information)
    {
        CNonlinearSolverType &solver = m_p_optimizer->r_Solver();
        solver.Incremental_Step(Add_Edge(n_vertex_id1, n_vertex_id2, v_measurement, t_information));
    }


    virtual void SaveSolution(const char *p_s_filename)
    {
        CSystemType &system = m_p_optimizer->r_System();
        _TyVertexMultiPool &vertex_pool = system.r_Vertex_Pool();
        std::FILE * output_file;
        output_file = std::fopen(p_s_filename, "w");
        for (size_t i = 0, n = system.r_Vertex_Pool().n_Size(); i < n; ++i) {
            _TyVertexPose &vertex = vertex_pool.template r_At<_TyVertexPose>(i);
            _TyVector v = vertex.v_State();
            if(b_2D_SLAM){
                std::fprintf(output_file,
                             "VERTEX2 %zu %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2]
                );
            }else{
                std::fprintf(output_file,
                             "VERTEX3 %zu %.8f %.8f %.8f %.8f %.8f %.8f\n",
                             i, v[0], v[1], v[2], v[3], v[4], v[5]
                );
            }
        }
        std::fclose(output_file);
        printf("Estimates of vertices have already been saved into the file: \"%s\" \n", p_s_filename);
    }


    virtual void PlotTrajectory(const char *p_s_filename = NULL)
    {
        if(p_s_filename == NULL)
            p_s_filename = "default.tga";
        CSystemType &system = m_p_optimizer->r_System();
        if(b_2D_SLAM){
            system.Plot2D(p_s_filename, plot_quality::plot_Printing); // plot in print quality
        }else{
            system.Plot3D(p_s_filename, plot_quality::plot_Printing); // plot in print quality
        }
        printf("The trajectory has been plotted in the file: \"%s\" \n", p_s_filename);
    }


    virtual void SaveSummary(const char *p_s_filename)
    {
        //tic.Accum_DiffSample(f_m_total_time);
        tic.Accum_CumTime_LastSample(f_m_total_time);
        std::FILE * summary_file;
        summary_file = std::fopen(p_s_filename, "w");
        std::fprintf(summary_file, "n_vertex = %zu\n", n_Vertex_Num());
        std::fprintf(summary_file, "n_edge = %zu\n", n_Edge_Num());
        std::fprintf(summary_file, "TotalTiming = %.8f\n", f_m_total_time);
        std::fclose(summary_file);
    }

private:






};



// initialized as a batch optimization core
template <const bool b_2D_SLAM, const bool b_Incremental_Solver> template <bool const t_b_2D_SLAM>
class CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::CPoseGraphOptimizerCore<t_b_2D_SLAM, false> {

public:

    typedef typename CChooseType<CVertexPose2D, CVertexPose3D, t_b_2D_SLAM>::_TyResult _TyVertexPose; /**< @brief pose vertex type */
    typedef typename CChooseType<CEdgePose2D, CEdgePose3D, t_b_2D_SLAM>::_TyResult _TyEdgePose; /**< @brief pose-pose edge type */

    typedef typename MakeTypelist(_TyVertexPose) TVertexTypelist; /**< @brief list of vertex types */
    typedef typename MakeTypelist(_TyEdgePose) TEdgeTypelist; /**< @brief list of edge types */

    typedef CFlatSystem<_TyVertexPose, TVertexTypelist, _TyEdgePose,
            TEdgeTypelist, CProportionalUnaryFactorFactory> CSystemType; /**< @brief optimized system type */
    typedef CLinearSolver_UberBlock<typename CSystemType::_TyHessianMatrixBlockList> CLinearSolverType; /**< @brief linear solver type */

    typedef CNonlinearSolver_Lambda<CSystemType, CLinearSolverType> CNonlinearSolverType; /**< @brief nonlinear solver type */


protected:
    CSystemType m_system;
    CNonlinearSolverType m_solver;


public:
    inline CPoseGraphOptimizerCore(size_t n_max_nonlinear_iteration_num = 5, double f_nonlinear_error_thresh = .001)
        :m_solver(m_system,
                  solve::Nonlinear(frequency::Every(1), n_max_nonlinear_iteration_num, f_nonlinear_error_thresh),
                  TMarginalsComputationPolicy(marginals::do_calculate,
                                              frequency::Every(1),
                                              EBlockMatrixPart(mpart_LastColumn | mpart_Diagonal),
                                              EBlockMatrixPart(mpart_LastColumn | mpart_Diagonal)
                  ),
                  false /*verbose*/)
    {
        printf("Initialized as a batch optimizer core!\n");
    }

    inline CSystemType &r_System()
    {
        return m_system;
    }

    inline CNonlinearSolverType &r_Solver()
    {
        return m_solver;
    }
};



// initialized as an incremental optimization core
template <const bool b_2D_SLAM, const bool b_Incremental_Solver> template <bool const t_b_2D_SLAM>
class CPoseGraphOptimizer<b_2D_SLAM, b_Incremental_Solver>::CPoseGraphOptimizerCore<t_b_2D_SLAM, true> {

public:

    typedef typename CChooseType<CVertexPose2D, CVertexPose3D, t_b_2D_SLAM>::_TyResult _TyVertexPose; /**< @brief pose vertex type */
    typedef typename CChooseType<CEdgePose2D, CEdgePose3D, t_b_2D_SLAM>::_TyResult _TyEdgePose; /**< @brief pose-pose edge type */

    typedef typename MakeTypelist(_TyVertexPose) TVertexTypelist; /**< @brief list of vertex types */
    typedef typename MakeTypelist(_TyEdgePose) TEdgeTypelist; /**< @brief list of edge types */

    typedef CFlatSystem<_TyVertexPose, TVertexTypelist, _TyEdgePose,
            TEdgeTypelist, CProportionalUnaryFactorFactory> CSystemType; /**< @brief optimized system type */
    typedef CLinearSolver_UberBlock<typename CSystemType::_TyHessianMatrixBlockList> CLinearSolverType; /**< @brief linear solver type */

    typedef CNonlinearSolver_FastL<CSystemType, CLinearSolverType> CNonlinearSolverType; /**< @brief nonlinear solver type */


protected:
    CSystemType m_system;
    CNonlinearSolverType m_solver;


public:
    inline CPoseGraphOptimizerCore(size_t n_max_nonlinear_iteration_num = 5, double f_nonlinear_error_thresh = .001)
            :m_solver(m_system,
                      solve::Nonlinear(frequency::Every(1), n_max_nonlinear_iteration_num, f_nonlinear_error_thresh),
                      TMarginalsComputationPolicy(marginals::do_calculate,
                                                  frequency::Every(1),
                                                  EBlockMatrixPart(mpart_LastColumn | mpart_Diagonal),
                                                  EBlockMatrixPart(mpart_LastColumn | mpart_Diagonal)
                      ),
                      false /*verbose*/)
    {
        printf("Initialized as an incremental optimizer core!\n");
    }

    inline CSystemType &r_System()
    {
        return m_system;
    }

    inline CNonlinearSolverType &r_Solver()
    {
        return m_solver;
    }
};



#endif //SLAM_PLUS_PLUS_POSEGRAPHOPTIMIZER_H
