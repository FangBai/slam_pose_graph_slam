//
// Created by fang on 2/01/18.
//


#include <iostream>
#include <fstream>

#include "PoseGraphOptimizerOnline.h"
#include "Stuff.h"



int main(int n_arg_num, const char **p_arg_list)
{

    typedef CPoseGraphOptimizerOnline<true, false> Optimizer2D_Batch;
    typedef CPoseGraphOptimizerOnline<false, false> Optimizer3D_Batch;


    TParams params;
    if(params.n_ParseCommandline(n_arg_num, p_arg_list) < 0)
        return -1;


    const bool b_2D_data = params.b_2D_SLAM;
    const bool b_Incremental_Solve = params.b_incremental_solve;


    printf("b_2D_SLAM = %s \t b_Incremental_Solver = %s \n",
           b_2D_data ? "true" : "false", b_Incremental_Solve ? "true" : "false");

    try{
        if(b_2D_data){
            if(!b_Incremental_Solve){
                std::vector<TEdgeData2D> edges;
                if (!Load_Dataset(params.p_s_input_file, edges)){
                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
                    return -1;
                }
                printf("size of the edges = %zu \n", edges.size());
                //** ----------------------------------------- **//
                // Initialize pose graph online optimizer
                Optimizer2D_Batch optimizer(20, .001);
                // feed data into the optimizer
                for(size_t i = 0, n = edges.size(); i < n; ++ i)
                {
                    const TEdgeData2D &r_edge = edges[i];
                    optimizer.Add_Edge(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
                }
                optimizer.Optimize(100, 0.00001);
                printf("All edges has been fed into the optimizer! \n");
                if(params.p_s_vertices_file != NULL)
                    optimizer.SaveSolution(params.p_s_vertices_file);
                if(params.p_s_objFunc_file !=NULL)
                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
                if(params.p_s_summary_file !=NULL)
                    optimizer.SaveSummary(params.p_s_summary_file);
                optimizer.PlotTrajectory("after.tga");
                return 0;
            }
            else{
                std::vector<TEdgeData2D> edges;
                if (!Load_Dataset(params.p_s_input_file, edges)) {
                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
                    return -1;
                }
                printf("size of the edges = %zu \n", edges.size());
                //** ----------------------------------------- **//
                // Initialize pose graph online optimizer
                Optimizer2D_Batch optimizer(20, .001);
                // feed data into the optimizer
                for(size_t i = 0, n = edges.size(); i < n; ++ i)
                {
                    const TEdgeData2D &r_edge = edges[i];
                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
                }
                printf("All edges has been fed into the optimizer! \n");
                if(params.p_s_vertices_file != NULL)
                    optimizer.SaveSolution(params.p_s_vertices_file);
                if(params.p_s_objFunc_file !=NULL)
                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
                if(params.p_s_summary_file !=NULL)
                    optimizer.SaveSummary(params.p_s_summary_file);
                optimizer.PlotTrajectory("after.tga");
                return 0;
            }
        }else{
            if(!b_Incremental_Solve){
                std::vector<TEdgeData3D> edges;
                if (!Load_Dataset(params.p_s_input_file, edges)){
                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
                    return -1;
                }
                printf("size of the edges = %zu \n", edges.size());
                //** ----------------------------------------- **//
                // Initialize pose graph online optimizer
                Optimizer3D_Batch optimizer(20, .001);
                // feed data into the optimizer
                for(size_t i = 0, n = edges.size(); i < n; ++ i)
                {
                    const TEdgeData3D &r_edge = edges[i];
                    optimizer.Add_Edge(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
                }
                optimizer.Optimize(100, 0.00001);
                printf("All edges has been fed into the optimizer! \n");
                if(params.p_s_vertices_file != NULL)
                    optimizer.SaveSolution(params.p_s_vertices_file);
                if(params.p_s_objFunc_file !=NULL)
                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
                if(params.p_s_summary_file !=NULL)
                    optimizer.SaveSummary(params.p_s_summary_file);
                optimizer.PlotTrajectory("after.tga");
                return 0;
            }
            else{
                std::vector<TEdgeData3D> edges;
                if (!Load_Dataset(params.p_s_input_file, edges)) {
                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
                    return -1;
                }
                printf("size of the edges = %zu \n", edges.size());
                //** ----------------------------------------- **//
                // Initialize pose graph online optimizer
                Optimizer3D_Batch optimizer(20, .001);
                // feed data into the optimizer
                for(size_t i = 0, n = edges.size(); i < n; ++ i)
                {
                    const TEdgeData3D &r_edge = edges[i];
                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
                }
                printf("All edges has been fed into the optimizer! \n");
                if(params.p_s_vertices_file != NULL)
                    optimizer.SaveSolution(params.p_s_vertices_file);
                if(params.p_s_objFunc_file !=NULL)
                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
                if(params.p_s_summary_file !=NULL)
                    optimizer.SaveSummary(params.p_s_summary_file);
                optimizer.PlotTrajectory("after.tga");
                return 0;
            }
        }
    } catch(std::exception &r_exc) {
        fprintf(stderr, "error: uncaught exception: \"%s\"\n", r_exc.what());
        return -1;
    }


}
