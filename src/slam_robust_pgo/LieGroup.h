//
// Created by fang on 4/01/18.
//

#pragma once
#ifndef SLAM_PLUS_PLUS_LIEGROUP_H
#define SLAM_PLUS_PLUS_LIEGROUP_H

#include "eigen/Eigen/Core"
#include <iostream>


// forward class declarations
class SO2;
class SO3;
class SE2;
class SE3;


// Specialization for 2D cases
template <const bool b_2D_Flag = true>
class LieGroup
{
private:
    LieGroup();

public:
    typedef SO2 _TySO;
    typedef SE2 _TySE;
};


// Specialization for 3D cases
template <>
class LieGroup<false>
{
private:
    LieGroup();

public:
    typedef SO3 _TySO;
    typedef SE3 _TySE;
};







// inject the 6D types into Eigen namespace
namespace Eigen {

#ifndef HAVE_EIGEN_6D_DOUBLE_VECTOR
    #define HAVE_EIGEN_6D_DOUBLE_VECTOR
typedef Eigen::Matrix<double, 6, 1> Vector6d; /**< @brief a 6D vector */
#endif // !HAVE_EIGEN_6D_DOUBLE_VECTOR
#ifndef HAVE_EIGEN_6D_DOUBLE_MATRIX
    #define HAVE_EIGEN_6D_DOUBLE_MATRIX
typedef Eigen::Matrix<double, 6, 6> Matrix6d; /**< @brief a 6x6 matrix */
#endif // !HAVE_EIGEN_6D_DOUBLE_MATRIX

} // ~Eigen





// function to check the sign of a value
template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

//*** implementaton of Lie Group operations ***//


/*
 * Implementation for SO(3) and SE(3)
 * Tested on January 19, 2018
 */


class SO3 {

private:

    SO3(){}

public:

    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Matrix4d Matrix4d;
    typedef Eigen::Vector3d Vector3d;
    typedef Eigen::Matrix6d Matrix6d;
    typedef Eigen::Vector6d Vector6d;


public:


    inline static Eigen::Matrix3d SkewSym(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }


    inline static Eigen::Matrix3d Hat(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }


    inline static Eigen::Matrix3d Exp(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double A = 1;
        double B = .5;
        if(angle > 1e-6){
            A = std::sin(angle)/angle;
            B = (1 - std::cos(angle))/(angle*angle);
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() + A * vHat + B * vHat * vHat );
    }


    inline static Eigen::Vector3d Log(const Eigen::Matrix3d & Rmatr)
    {
        double tmp = (Rmatr.trace()-1)/2;
        tmp = fmin(fmax(tmp,-1.0),1.0);
        double angle = std::acos(tmp);
        // do this for numerical round-off errors
        if(isnan(angle)){
            printf("numerical round-off errors happened at std::acos(), angle = NAN   @ SO3::Log()\n");
        }
        Eigen::Vector3d axis(0, 0, 1);
        if (M_PI - angle < 1e-6){  // if the angle is close to pi
//            printf("angle is close to PI: angle = %f   @ SO3::Log() ", angle);
            axis(0) = - sgn(Rmatr(1,2)) * std::sqrt((1+Rmatr(0,0))/2);
            axis(1) = - sgn(Rmatr(0,2)) * std::sqrt((1+Rmatr(1,1))/2);
            axis(2) = - sgn(Rmatr(0,1)) * std::sqrt((1+Rmatr(2,2))/2);
        } else if (angle > 1e-6){
            double sina2 = 2 * std::sin(angle);
            axis(0) = (Rmatr(2,1) - Rmatr(1,2))/sina2;
            axis(1) = (Rmatr(0,2) - Rmatr(2,0))/sina2;
            axis(2) = (Rmatr(1,0) - Rmatr(0,1))/sina2;
        }
        return ( angle * axis );
    }


    inline static Eigen::Matrix3d Jl(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double B = 0.5;
        double C = 1/6;
        if(angle > 1e-6){
            B = (1 - std::cos(angle))/(angle*angle);
            C = (angle - std::sin(angle))/(angle*angle*angle);
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() + B * vHat + C * vHat * vHat );
    }


    inline static Eigen::Matrix3d InvJl(const Eigen::Vector3d & Rvec)
    {
        double angle = Rvec.norm();
        double D = 1/12;
        if(angle > 1e-6){
            D = (1/angle - std::sin(angle)/(2*(1-std::cos(angle))))/angle;
        }
        Eigen::Matrix3d vHat = SO3::SkewSym(Rvec);
        return ( Eigen::Matrix3d::Identity() - 0.5 * vHat + D * vHat * vHat );
    }


    inline static Eigen::Matrix3d Jr(const Eigen::Vector3d & Rvec)
    {
        return SO3::Jl(-Rvec);
    }


    inline static Eigen::Matrix3d InvJr(const Eigen::Vector3d & Rvec)
    {
        return SO3::InvJl(-Rvec);
    }


    // Some trivial operations that is not usually necessary
    inline static Eigen::Matrix3d Inv(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr.transpose();  /* use transposeInPlace() for A = A'; */
    }


    inline static Eigen::Matrix3d Ad(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr;
    }


    inline static Eigen::Matrix3d InvAd(const Eigen::Matrix3d & Rmatr)
    {
        return Rmatr.transpose(); /* use transposeInPlace() for A = A'; */
    }


};


class SE3 {

private:

    SE3(){}

public:

    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Matrix4d Matrix4d;
    typedef Eigen::Vector3d Vector3d;
    typedef Eigen::Matrix6d Matrix6d;
    typedef Eigen::Vector6d Vector6d;


public:


    inline static Eigen::Matrix3d SkewSym(const Eigen::Vector3d & v)
    {
        Eigen::Matrix3d m;
        m <<   0,  -v(2), v(1),
              v(2),  0,  -v(0),
             -v(1), v(0),  0;
        return m;
    }

    // transform from vector form storage to matrix form storage
    inline static Eigen::Matrix4d V2M (const Eigen::Vector6d & v_rt)
    {
        Eigen::Matrix4d m_Rt = Eigen::Matrix4d::Identity();
        m_Rt.topLeftCorner<3,3>() = SO3::Exp(v_rt.tail<3>());
        m_Rt.topRightCorner<3,1>() = v_rt.head<3>();
        return m_Rt;
    }

    // transform from matrix form storage to vector form storage
    inline static Eigen::Vector6d M2V (const Eigen::Matrix4d & m_Rt)
    {
        Eigen::Vector6d v_rt;
        v_rt.head<3>() = m_Rt.topRightCorner<3,1>();
        v_rt.tail<3>() = SO3::Log(m_Rt.topLeftCorner<3,3>());
        return v_rt;
    }


    inline static Eigen::Matrix4d Exp (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix4d m = Eigen::Matrix4d::Identity();
        m.topLeftCorner<3,3>() = SO3::Exp(Tvec.tail<3>());
        m.topRightCorner<3,1>() = SO3::Jl(Tvec.tail<3>()) * Tvec.head<3>();
        return m;
    }


    inline static Eigen::Vector6d Log (const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Vector6d v;
        v.tail<3>() = SO3::Log(Tmatr.topLeftCorner<3,3>());
        v.head<3>() = SO3::InvJl(v.tail<3>()) * Tmatr.topRightCorner<3,1>();
        return v;
    }


    // Intermediate matrix for Jl, InvJl
    inline static Eigen::Matrix3d Ql (const Eigen::Vector6d & Tvec)
    {
        double angle = Tvec.tail<3>().norm();
        double C = 1/6;
        double E = -1/24;
        double F = -1/60;
        if(angle > 1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            C = (angle - sina)/(angle*angle*angle);
            E = (1 - 0.5 * angle*angle - cosa)/(angle*angle*angle*angle);
            F = E - (6*C-1)/(2*angle*angle);
        }
        Eigen::Matrix3d uHat = SO3::SkewSym(Tvec.head<3>());
        Eigen::Matrix3d wHat = SO3::SkewSym(Tvec.tail<3>());
        Eigen::Matrix3d m;
        m = 0.5 * uHat
                + C * ( wHat*uHat + uHat*wHat + wHat*uHat*wHat )
                - E * ( wHat*wHat*uHat + uHat*wHat*wHat - 3 * wHat*uHat*wHat)
                - 0.5 * F * ( wHat*uHat*wHat*wHat + wHat*wHat*uHat*wHat );
        return m;
    }


    inline static Eigen::Matrix6d Jl (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix6d m;
        Eigen::Matrix3d t_Jlw = SO3::Jl(Tvec.tail<3>());
        m.topLeftCorner<3,3>() = t_Jlw;
        m.topRightCorner<3,3>() = SE3::Ql(Tvec);
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = t_Jlw;
        return m;
    }


    inline static Eigen::Matrix6d InvJl (const Eigen::Vector6d & Tvec)
    {
        Eigen::Matrix6d m;
        Eigen::Matrix3d t_InvJlw = SO3::InvJl(Tvec.tail<3>());
        m.topLeftCorner<3,3>() = t_InvJlw;
        m.topRightCorner<3,3>() = - t_InvJlw * SE3::Ql(Tvec) * t_InvJlw;
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = t_InvJlw;
        return m;
    }


    inline static Eigen::Matrix6d Jr (const Eigen::Vector6d & Tvec)
    {
        return SE3::Jl(-Tvec);
    }


    inline static Eigen::Matrix6d InvJr (const Eigen::Vector6d & Tvec)
    {
        return SE3::InvJl(-Tvec);
    }


    inline static Eigen::Matrix4d Inv(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d invR = Tmatr.block<3, 3>(0, 0).transpose();
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix4d m = Eigen::Matrix4d::Identity();
        m.topLeftCorner<3,3>() = invR;
        m.topRightCorner<3,1>() = -invR * t;
        return m;
    }


    inline static Eigen::Matrix6d Ad(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d R = Tmatr.block<3, 3>(0, 0);
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix6d m;
        m.topLeftCorner<3,3>() = R;
        m.topRightCorner<3,3>() = SO3::SkewSym(t) * R;
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = R;
        return m;
    }


    inline static Eigen::Matrix6d InvAd(const Eigen::Matrix4d & Tmatr)
    {
        Eigen::Matrix3d invR = Tmatr.block<3, 3>(0, 0).transpose();
        Eigen::Vector3d t = Tmatr.block<3, 1>(0, 3);
        Eigen::Matrix6d m;
        m.topLeftCorner<3,3>() = invR;
        m.topRightCorner<3,3>() = -invR * SO3::SkewSym(t);
        m.bottomLeftCorner<3,3>().setZero();
        m.bottomRightCorner<3,3>() = invR;
        return m;
    }


};


/*
 * Implementation for SO(2) and SE(2)
 * Tested on January 7, 2018
 */


class SO2 {

private:
    SO2(){}

public:

    typedef Eigen::Matrix2d Matrix2d;
    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Vector2d Vector2d;
    typedef Eigen::Vector3d Vector3d;


public:


    inline static Eigen::Matrix2d SkewSym(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }


    inline static Eigen::Matrix2d Hat(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }


    inline static Eigen::Matrix2d Exp(double angle)
    {
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        Eigen::Matrix2d m;
        m << cosa, -sina,
             sina,  cosa;
        return m;
    }


    inline static double Log(const Eigen::Matrix2d & Rmatr)
    {
        double sina = Rmatr(1,0);
        double cosa = Rmatr(0,0);
        return std::atan2(sina, cosa);
    }


    // Some trivial operations that is not usually necessary
    inline static Eigen::Matrix2d Inv(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr.transpose();  /* use transposeInPlace() for A = A'; */
    }


    inline static Eigen::Matrix2d Ad(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr;
    }


    inline static Eigen::Matrix2d InvAd(const Eigen::Matrix2d & Rmatr)
    {
        return Rmatr.transpose(); /* use transposeInPlace() for A = A'; */
    }


};


class SE2 {

private:

    SE2(){}

public:

    typedef Eigen::Matrix2d Matrix2d;
    typedef Eigen::Matrix3d Matrix3d;
    typedef Eigen::Vector2d Vector2d;
    typedef Eigen::Vector3d Vector3d;


public:


    // transform from Vector form storage to Matrix form storage
    inline static Eigen::Matrix3d V2M(const Eigen::Vector3d & v_rt)
    {
        double angle = v_rt(2);
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        Eigen::Matrix3d m;
        m << cosa, -sina, v_rt(0),
             sina,  cosa, v_rt(1),
             0,     0,    1;
        return m;
    }


    // transform from Matrix form storage to Vector form storage
    inline static Eigen::Vector3d M2V(const Eigen::Matrix3d & m_Rt)
    {
        double sina = m_Rt(1,0);
        double cosa = m_Rt(0,0);
        double angle = std::atan2(sina, cosa);
        Eigen::Vector3d v_rt( m_Rt(0,2),
                              m_Rt(1,2),
                              angle);
        return v_rt;
    }



    inline static Eigen::Matrix2d SkewSym(double angle)
    {
        Eigen::Matrix2d m;
        m << 0,    -angle,
             angle, 0;
        return m;
    }



    inline static Eigen::Matrix3d Exp(const Eigen::Vector3d & Tvec)
    {
        double angle = Tvec(2);
        double sina = std::sin(angle);
        double cosa = std::cos(angle);
        double A = 1;
        double B = 0;
        if(std::abs(angle)>1e-6){
            A = sina/angle;
            B = (1-cosa)/angle;
        }
        Eigen::Matrix3d m;
        m << cosa, -sina, A*Tvec(0)-B*Tvec(1),
             sina,  cosa, B*Tvec(0)+A*Tvec(1),
             0,     0,    1;
        return m;
    }



    inline static Eigen::Vector3d Log(const Eigen::Matrix3d & Tmatr)
    {
        double sina = Tmatr(1,0);
        double cosa = Tmatr(0,0);
        double angle = std::atan2(sina, cosa);
        double iA = 1;
        double iB = 0;
        if(std::abs(angle)>1e-6){
            iA = angle*sina/(2-2*cosa);
            iB = angle/2;
        }
        Eigen::Vector3d v( iA*Tmatr(0,2) + iB*Tmatr(1,2),
                          -iB*Tmatr(0,2) + iA*Tmatr(1,2),
                           angle);
        return v;
    }



    inline static Eigen::Matrix3d Jl(const Eigen::Vector3d & Tvec)
    {
        double x = Tvec(0);
        double y = Tvec(1);
        double angle = Tvec(2);
        double A = 1;
        double B = 0;
        double H1 = 0;
        double H2 = 0;
        if(std::abs(angle)>1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            A = sina/angle;
            B = (1-cosa)/angle;
            double aa = angle*angle;
            H1 = (angle-sina)/aa;
            H2 = (cosa + aa/2 - 1)/aa;
        }
        double q1 =  y/2 + H1*x - H2*y;
        double q2 = -x/2 + H2*x + H1*y;
        Eigen::Matrix3d m;
        m << A, -B, q1,
             B,  A, q2,
             0,  0,  1;
        return m;
    }



    inline static Eigen::Matrix3d InvJl(const Eigen::Vector3d & Tvec)
    {
        double x = Tvec(0);
        double y = Tvec(1);
        double angle = Tvec(2);
        double iA = 1;
        double iB = 0;
        double H1 = 0;
        double H2 = 0;
        if(std::abs(angle)>1e-6){
            double sina = std::sin(angle);
            double cosa = std::cos(angle);
            iA = angle*sina/(2-2*cosa);
            iB = angle/2;
            double aa = angle*angle;
            H1 = (angle-sina)/aa;
            H2 = (cosa + aa/2 - 1)/aa;
        }
        double q1 =  y/2 + H1*x - H2*y;
        double q2 = -x/2 + H2*x + H1*y;
        double iq1 =  iA * q1 + iB * q2;
        double iq2 = -iB * q1 + iA * q2;
        Eigen::Matrix3d m;
        m << iA, iB, -iq1,
            -iB, iA, -iq2,
              0,  0,  1;
        return m;
    }



    inline static Eigen::Matrix3d Jr(const Eigen::Vector3d & Tvec)
    {
        return SE2::Jl(-Tvec);
    }



    inline static Eigen::Matrix3d InvJr(const Eigen::Vector3d & Tvec)
    {
        return SE2::InvJl(-Tvec);
    }



    inline static Eigen::Matrix3d Inv(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,0)*Tmatr(0,2)-Tmatr(1,0)*Tmatr(1,2),
             Tmatr(0,1), Tmatr(1,1), -Tmatr(0,1)*Tmatr(0,2)-Tmatr(1,1)*Tmatr(1,2),
             0,          0,           1;
        return m;
    }



    static Eigen::Matrix3d Ad(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(0,1),  Tmatr(1,2),
             Tmatr(1,0), Tmatr(1,1), -Tmatr(0,2),
             0,          0,           1;
        return m;
    }



    static Eigen::Matrix3d InvAd(const Eigen::Matrix3d & Tmatr)
    {
        Eigen::Matrix3d m;
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,0)*Tmatr(1,2)+Tmatr(1,0)*Tmatr(0,2),
             Tmatr(0,1), Tmatr(1,1), -Tmatr(0,1)*Tmatr(1,2)+Tmatr(1,1)*Tmatr(0,2),
             0,          0,           1;
        // an equivalent expression
        /*
        m << Tmatr(0,0), Tmatr(1,0), -Tmatr(0,1)*Tmatr(0,2)-Tmatr(1,1)*Tmatr(1,2),
             Tmatr(0,1), Tmatr(1,1),  Tmatr(0,0)*Tmatr(0,2)+Tmatr(1,0)*Tmatr(1,2),
             0,          0,           1;
        */
        return m;
    }


};



#endif //SLAM_PLUS_PLUS_LIEGROUP_H
