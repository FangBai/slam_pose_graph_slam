//
// Created by fang on 9/01/18.
//

#ifndef SLAM_PLUS_PLUS_STUFF_H
#define SLAM_PLUS_PLUS_STUFF_H



/**
 *	@brief parsed edge data
 *	@tparam n_dimension is measurement dimension (either 3 for 2D or 6 for 3D)
 */
template <const int n_dimension = 3>
struct TEdgeData {
    typedef Eigen::Matrix<double, n_dimension, n_dimension, Eigen::DontAlign> _TyMatrix; /**< @brief matrix type */
    typedef Eigen::Matrix<double, n_dimension, 1, Eigen::DontAlign> _TyVector; /**< @brief vector type */
    size_t p_vertex[2]; /**< @brief indices of the vertices */
    _TyVector v_measurement; /**< @brief measurement vector */
    _TyMatrix t_information; /**< @brief information matrix */

    /**
     *	@brief default constructor; has no effect
     */
    TEdgeData()
    {}

    /**
     *	@brief constructor; initializes the edge data
     *
     *	@param[in] n_vertex_0 is index of the first vertex
     *	@param[in] n_vertex_1 is index of the second vertex
     *	@param[in] r_v_measurement is measurement vector
     *	@param[in] r_t_information is information matrix
     */
    TEdgeData(size_t n_vertex_0, size_t n_vertex_1,
              const _TyVector &r_v_measurement, const _TyMatrix &r_t_information)
            :v_measurement(r_v_measurement), t_information(r_t_information)
    {
        p_vertex[0] = n_vertex_0;
        p_vertex[1] = n_vertex_1;
    }

    /**
     *	@brief comparison operator
     *	@param[in] r_edge is an edge
     *	@return Returns true if this edge connects the same two vertices
     *		as r_edge does (in any order), otherwise returns false.
     */
    bool operator ==(const std::pair<size_t, size_t> &r_edge) const
    {
        return (p_vertex[0] == r_edge.first && p_vertex[1] == r_edge.second) ||
               (p_vertex[1] == r_edge.first && p_vertex[0] == r_edge.second);
    }
};

typedef TEdgeData<3> TEdgeData2D; /**< @brief 2D edge data type */
typedef TEdgeData<6> TEdgeData3D; /**< @brief 3D edge data type */


/**
 *	@brief a helper function that detects dataset type
 *
 *	@param[in] p_s_filename is null-terminated string, containing file name of a dataset
 *	@param[out] r_b_is_2D is dataset type flag (filled upon successful return, true means a 2D dataset, false means 3D)
 *
 *	@return Returns true on success, false on failure (failure both to parse the file or to determine dataset type).
 */
static bool Detect_DatasetType(const char *p_s_filename, bool &r_b_is_2D);

/**
 *	@brief a helper function that loads a 2D dataset
 *
 *	@param[in] p_s_filename is null-terminated string, containing file name of a dataset
 *	@param[out] edges is vector of edges to add the edges to
 *
 *	@return Returns true on success, false on failure.
 */
static bool Load_Dataset(const char *p_s_filename, std::vector<TEdgeData2D> &edges);

/**
 *	@brief a helper function that loads a 3D dataset
 *
 *	@param[in] p_s_filename is null-terminated string, containing file name of a dataset
 *	@param[out] edges is vector of edges to add the edges to
 *
 *	@return Returns true on success, false on failure.
 */
static bool Load_Dataset(const char *p_s_filename, std::vector<TEdgeData3D> &edges);


/**
 *	@brief creates a directory
 *	@param[in] p_s_path is null-terminated string containing the name of the directory to create
 *	@return Returns zero on success, nonzero on failure.
 */
static int MakeDir(const char *p_s_path);


int MakeDir(const char *p_s_path)
{
#if defined(_WIN32) || defined(_WIN64)
    return system(((std::string("mkdir ") + p_s_path) + " > nul 2>&1").c_str());
#else // _WIN32 || _WIN64
    return system(((std::string("mkdir ") + p_s_path) + " > /dev/null 2>&1").c_str());
#endif // _WIN32 || _WIN64
}










//** Functions to Manipulate Data

/**
 *	@brief a simple parse loop
 *
 *	@tparam CEdgeData is edge data type to store
 *	@tparam CEdgeParseType is parsed edge type
 */
template <class CEdgeData = TEdgeData2D, class CEdgeParseType = CParserBase::TEdge2D>
class CMyParseLoop {
protected:
    std::vector<CEdgeData> &m_r_edges; /**< @brief reference to the output vector of edges */

public:
    /**
     *	@brief default constructor
     *	@param[in] r_edges is reference to the output vector of edges
     */
    CMyParseLoop(std::vector<CEdgeData> &r_edges)
            :m_r_edges(r_edges)
    {}

    /**
     *	@brief appends the system with a measurement
     *	@param[in] r_t_edge is the measurement to be appended
     */
    void AppendSystem(const CEdgeParseType &r_t_edge) // throw(std::bad_alloc)
    {
        m_r_edges.push_back(CEdgeData(r_t_edge.m_n_node_0, r_t_edge.m_n_node_1,
                                      r_t_edge.m_v_delta, r_t_edge.m_t_inv_sigma));
    }
};

/**
 *	@brief a simple parse loop
 */
class CMyDatasetTypeDetector {
protected:
    bool m_b_has_2D; /**< @brief 2D edges presence flag */
    bool m_b_has_3D; /**< @brief 3D edges presence flag */
    bool m_b_has_other; /**< @brief other edges / vertices presence flag */

public:
    /**
     *	@brief default constructor
     */
    CMyDatasetTypeDetector()
            :m_b_has_2D(false), m_b_has_3D(false), m_b_has_other(false)
    {}

    /**
     *	@brief determines whether the type of the dataset is clean cut
     *	@return Returns true if the dataset contains either purely
     *		2D or purely 3D edges, otherwise returns false.
     */
    bool b_DeterminateType() const
    {
        return !(m_b_has_other || (m_b_has_2D && m_b_has_3D) || (!m_b_has_2D && !m_b_has_3D));
        // if there are other types than 2D or if it mixes 2D and 3D or if it does not
        // have either 2D or 3D, then the type cannot be classified as purely 2D or 3D
    }

    /**
     *	@brief determines whether this dataset is a 2D dataset
     *	@return Returns true if the dataset is 2D, otherwise returns false.
     *	@note In case b_DeterminateType() returns true, then the dataset
     *		is 2D if this returns true or 3D if this returns false. Otherwise
     *		the type is unclear / other.
     */
    bool b_Is_2D() const
    {
        return m_b_has_2D;
    }

    /**
     *	@brief appends the system with a 2D measurement
     *	@param[in] r_t_edge is the measurement to be appended (value unused)
     */
    void AppendSystem(const CParserBase::TEdge2D &UNUSED(r_t_edge))
    {
        m_b_has_2D = true;
    }

    /**
     *	@brief appends the system with a 3D measurement
     *	@param[in] r_t_edge is the measurement to be appended (value unused)
     */
    void AppendSystem(const CParserBase::TEdge3D &UNUSED(r_t_edge))
    {
        m_b_has_3D = true;
    }

    /**
     *	@brief appends the system with a general edge of unknown type
     *	@tparam COtherEdgeType is edge type, other than CParserBase::TEdge2D or CParserBase::TEdge3D
     *	@param[in] r_t_edge is the measurement to be appended (value unused)
     */
    template <class COtherEdgeType>
    void AppendSystem(const COtherEdgeType &UNUSED(r_t_edge))
    {
        m_b_has_other = true;
    }

    /**
     *	@brief initializes a vertex
     *	@tparam CVertexType is vertex type (any)
     *	@param[in] r_t_vertex is the vertex to be initialized (value unused)
     */
    template <class CVertexType>
    void InitializeVertex(const CVertexType &UNUSED(r_t_vertex))
    {
        m_b_has_other = true;
    }
};

bool Detect_DatasetType(const char *p_s_filename, bool &r_b_is_2D)
{
    typedef CParserTemplate<CMyDatasetTypeDetector, CStandardParsedPrimitives> CParser;

    CMyDatasetTypeDetector detect_type;
    if(!CParser().Parse(p_s_filename, detect_type, 100))
        return false;
    // use the first 100 lines to detect the type

    if(!detect_type.b_DeterminateType()) {
        fprintf(stderr, "error: file \'%s\': unsupported data type\n", p_s_filename);
        return false;
    }
    // can we clearly determine whether it is 2D or 3D?

    r_b_is_2D = detect_type.b_Is_2D();
    // read the type

    return true;
}

bool Load_Dataset(const char *p_s_filename, std::vector<TEdgeData2D> &r_edges)
{
    r_edges.clear(); // !!

    typedef MakeTypelist(CEdge2DParsePrimitive) AllowedEdges;
    typedef CParserTemplate<CMyParseLoop<>, AllowedEdges> CParser;

    CMyParseLoop<> ploop(r_edges);
    if(!CParser().Parse(p_s_filename, ploop))
        return false;

    printf("loaded \'%s\' : " PRIsize " edges\n", p_s_filename, r_edges.size());

    return true;
}

bool Load_Dataset(const char *p_s_filename, std::vector<TEdgeData3D> &r_edges)
{
    r_edges.clear(); // !!

    typedef MakeTypelist(CEdge3DParsePrimitive, CEdge3DParsePrimitiveAxisAngle) AllowedEdges;
    typedef CMyParseLoop<TEdgeData3D, CParserBase::TEdge3D> CMyParseLoop3D;
    typedef CParserTemplate<CMyParseLoop3D, AllowedEdges> CParser;

    CMyParseLoop3D ploop(r_edges);
    if(!CParser().Parse(p_s_filename, ploop))
        return false;

    printf("loaded \'%s\' : " PRIsize " edges\n", p_s_filename, r_edges.size());

    return true;
}




/**
 *	@brief struct ofr parameters
 */
struct TParams {

    const char *p_s_input_file;
    const char *p_s_objFunc_file;
    const char *p_s_vertices_file;
    const char *p_s_summary_file;
    bool b_2D_SLAM;
    bool b_incremental_solver;
    bool b_linearize_measurement;  // Either is good!!!

    TParams()
        :p_s_input_file(NULL), p_s_objFunc_file(NULL), p_s_vertices_file(NULL), p_s_summary_file(NULL),
         b_2D_SLAM(true), b_incremental_solver(false), b_linearize_measurement(false)
    {}

    void PrintHelp(){
        printf("use: slam_pose_graph <input-file> [options]\n\n"
               "where:\n"
               "--help | -h shows this help screen\n"
               "--incremental-solver | -is   initialize an incremental optimization core, default is a batch core \n"
               "--linearize-measurement | -lm   linearize measurements in formulas to predict objFunc change\n"
               "--objFunc-output-file | -of   output file of objective function changes and edge decision status\n"
               "--vertices-output-file | -vf   output file of states of vertices \n"
               "--summary-output-file | -sf   output file of summary information\n"
        );
    }

    int n_ParseCommandline(int n_arg_num, const char **p_arg_list){
        if(n_arg_num < 2) {
            fprintf(stderr, "error: the first argument must be the path to the input dataset\n");
            TParams::PrintHelp();
            return -1;
        }
        if(!strcmp(p_arg_list[1], "--help") || !strcmp(p_arg_list[1], "-h")){
            TParams::PrintHelp();
            return -2;
        }
        p_s_input_file = p_arg_list[1];
        if(!Detect_DatasetType(p_arg_list[1], b_2D_SLAM)) {
            fprintf(stderr, "error: failed to load \"%s\"\n", p_arg_list[1]);
            return -1;
        }
        for (int i = 2; i < n_arg_num; ++i) {
            if (!strcmp(p_arg_list[i], "--help") || !strcmp(p_arg_list[i], "-h")) {
                TParams::PrintHelp();
                return -2;
            } else if (!strcmp(p_arg_list[i], "--incremental-solver") || !strcmp(p_arg_list[i], "-is"))
                b_incremental_solver = true;
            else if (!strcmp(p_arg_list[i], "--linearize-measurement") || !strcmp(p_arg_list[i], "-lm"))
                b_linearize_measurement = true;
            else if (!strcmp(p_arg_list[i], "--objFunc-output-file") || !strcmp(p_arg_list[i], "-of"))
                p_s_objFunc_file = p_arg_list[++i];
            else if (!strcmp(p_arg_list[i], "--vertices-output-file") || !strcmp(p_arg_list[i], "-vf"))
                p_s_vertices_file = p_arg_list[++i];
            else if (!strcmp(p_arg_list[i], "--summary-output-file") || !strcmp(p_arg_list[i], "-sf"))
                p_s_summary_file = p_arg_list[++i];
            else {
                fprintf(stderr, "error: argument \"%s\": an unknown argument\n", p_arg_list[i]);
                return -1;
            }
        }
        return 0;
    }
};


#endif //SLAM_PLUS_PLUS_STUFF_H
