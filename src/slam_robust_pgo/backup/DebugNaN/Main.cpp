//
// Created by fang on 2/01/18.
//


#include <iostream>
#include <fstream>

#include "PoseGraphOptimizerOnline.h"
#include "Stuff.h"



int main(int n_arg_num, const char **p_arg_list)
{

    typedef CPoseGraphOptimizerOnline<true, false> Optimizer2D_Batch;
    typedef CPoseGraphOptimizerOnline<false, false> Optimizer3D_Batch;
    typedef CPoseGraphOptimizerOnline<true, true> Optimizer2D_Incremental;
    typedef CPoseGraphOptimizerOnline<false, true> Optimizer3D_Incremental;

    const char* p_s_filename = "data/incremental/sphere2500_incremental.txt";

    std::vector<TEdgeData3D> edges;
    if (!Load_Dataset(p_s_filename, edges)){
        printf("read file \"%s\" failed!\n", p_s_filename);
        return -1;
    }
    printf("size of the edges = %zu \n ", edges.size());
    //** ----------------------------------------- **//
    // Initialize pose graph online optimizer
    Optimizer3D_Batch optimizer(false, 50, .00001);
    // feed data into the optimizer
    for(size_t i = 0, n = edges.size(); i < n; ++ i)
    {
        const TEdgeData3D &r_edge = edges[i];
        optimizer.Add_Edge(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
    }
    printf("All edges has been added into the optimizer! \n");

    double objFunc1 = optimizer.Get_ObjFunc();
    optimizer.Optimize(50, .0001);
    double objFunc2 = optimizer.Get_ObjFunc();

    printf("ObjFunc1 = %f,   ObjFunc2 = %f \n", objFunc1, objFunc2);


    return 0;




//
//    TParams params;
//    if(params.n_ParseCommandline(n_arg_num, p_arg_list) < 0)
//        return -1;
//
//    const bool b_2D_data = params.b_2D_SLAM;
//    const bool b_Incremental_Solver = params.b_incremental_solver;
//
//    printf("b_2D_SLAM = %s \t b_Incremental_Solver = %s \n",
//           b_2D_data ? "true" : "false", b_Incremental_Solver ? "true" : "false");
//
//    try{
//        if(b_2D_data){
//            if(!b_Incremental_Solver){
//                std::vector<TEdgeData2D> edges;
//                if (!Load_Dataset(params.p_s_input_file, edges)){
//                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
//                    return -1;
//                }
//                printf("size of the edges = %zu \n ", edges.size());
//                //** ----------------------------------------- **//
//                // Initialize pose graph online optimizer
//                Optimizer2D_Batch optimizer(params.b_linearize_measurement, 20, .001);
//                // feed data into the optimizer
//                for(size_t i = 0, n = edges.size(); i < n; ++ i)
//                {
//                    const TEdgeData2D &r_edge = edges[i];
//                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
//                }
//                printf("All edges has been fed into the optimizer! \n");
//                if(params.p_s_vertices_file != NULL)
//                    optimizer.SaveSolution(params.p_s_vertices_file);
//                if(params.p_s_objFunc_file !=NULL)
//                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
//                if(params.p_s_summary_file !=NULL)
//                    optimizer.SaveSummary(params.p_s_summary_file);
//                return 0;
//            }
//            else{
//                std::vector<TEdgeData2D> edges;
//                if (!Load_Dataset(params.p_s_input_file, edges)) {
//                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
//                    return -1;
//                }
//                printf("size of the edges = %zu \n ", edges.size());
//                //** ----------------------------------------- **//
//                // Initialize pose graph online optimizer
//                Optimizer2D_Incremental optimizer(params.b_linearize_measurement, 20, .001);
//                // feed data into the optimizer
//                for(size_t i = 0, n = edges.size(); i < n; ++ i)
//                {
//                    const TEdgeData2D &r_edge = edges[i];
//                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
//                }
//                printf("All edges has been fed into the optimizer! \n");
//                if(params.p_s_vertices_file != NULL)
//                    optimizer.SaveSolution(params.p_s_vertices_file);
//                if(params.p_s_objFunc_file !=NULL)
//                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
//                if(params.p_s_summary_file !=NULL)
//                    optimizer.SaveSummary(params.p_s_summary_file);
//                return 0;
//            }
//        }else{
//            if(!b_Incremental_Solver){
//                std::vector<TEdgeData3D> edges;
//                if (!Load_Dataset(params.p_s_input_file, edges)){
//                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
//                    return -1;
//                }
//                printf("size of the edges = %zu \n ", edges.size());
//                //** ----------------------------------------- **//
//                // Initialize pose graph online optimizer
//                Optimizer3D_Batch optimizer(params.b_linearize_measurement, 20, .001);
//                // feed data into the optimizer
//                for(size_t i = 0, n = edges.size(); i < n; ++ i)
//                {
//                    const TEdgeData3D &r_edge = edges[i];
//                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
//                }
//                printf("All edges has been fed into the optimizer! \n");
//                if(params.p_s_vertices_file != NULL)
//                    optimizer.SaveSolution(params.p_s_vertices_file);
//                if(params.p_s_objFunc_file !=NULL)
//                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
//                if(params.p_s_summary_file !=NULL)
//                    optimizer.SaveSummary(params.p_s_summary_file);
//                return 0;
//            }
//            else{
//                std::vector<TEdgeData3D> edges;
//                if (!Load_Dataset(params.p_s_input_file, edges)) {
//                    printf("read file \"%s\" failed!\n", params.p_s_input_file);
//                    return -1;
//                }
//                printf("size of the edges = %zu \n ", edges.size());
//                //** ----------------------------------------- **//
//                // Initialize pose graph online optimizer
//                Optimizer3D_Incremental optimizer(params.b_linearize_measurement, 20, .001);
//                // feed data into the optimizer
//                for(size_t i = 0, n = edges.size(); i < n; ++ i)
//                {
//                    const TEdgeData3D &r_edge = edges[i];
//                    optimizer.FeedDataOnline(r_edge.p_vertex[0], r_edge.p_vertex[1], r_edge.v_measurement, r_edge.t_information);
//                }
//                printf("All edges has been fed into the optimizer! \n");
//                if(params.p_s_vertices_file != NULL)
//                    optimizer.SaveSolution(params.p_s_vertices_file);
//                if(params.p_s_objFunc_file !=NULL)
//                    optimizer.SaveEdgeInfoRecord(params.p_s_objFunc_file);
//                if(params.p_s_summary_file !=NULL)
//                    optimizer.SaveSummary(params.p_s_summary_file);
//                return 0;
//            }
//        }
//    } catch(std::exception &r_exc) {
//        fprintf(stderr, "error: uncaught exception: \"%s\"\n", r_exc.what());
//        return -1;
//    }


}
